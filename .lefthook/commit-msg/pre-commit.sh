#!/bin/bash

COMMIT_MSG_FILE="$1"
COMMIT_MSG=$(cat "$COMMIT_MSG_FILE")

REGEX="^(feat|fix|docs|style|refactor|test|chore|hotfix|ci): .{1,50}$"
BUMP_REGEX="^bump: v([0-9]+)\.([0-9]+)\.([0-9]+)(-[A-Z0-9]+)?$"

if [[ $COMMIT_MSG =~ $BUMP_REGEX ]]; then
    VERSION=${BASH_REMATCH[1]}.${BASH_REMATCH[2]}.${BASH_REMATCH[3]}
    if [[ -n ${BASH_REMATCH[4]} ]]; then
        VERSION+="${BASH_REMATCH[4]}"
    fi

    CHANGED_FILES=$(git diff --cached --name-only)
    EXPECTED_FILES=("pom.xml" "CHANGELOG.md" "Dockerfile")

    if [[ $(echo "$CHANGED_FILES" | wc -l) -ne 3 ]]; then
        echo "Erreur : Le commit de version ne doit contenir que les fichiers pom.xml et CHANGELOG.md et Dockerfile."
        echo "Fichiers trouvés :"
        echo "$CHANGED_FILES"
        exit 1
    fi

    for FILE in $CHANGED_FILES; do
        # shellcheck disable=SC2199
        if [[ ! " ${EXPECTED_FILES[@]} " =~ " ${FILE} " ]]; then
            echo "Erreur : Le commit de version ne doit contenir que les fichiers pom.xml, CHANGELOG.md et Dockerfile."
            echo "Fichier inattendu trouvé : $FILE"
            exit 1
        fi
    done

    # Vérification de la version dans pom.xml
    if ! grep -q "<version>$VERSION</version>" pom.xml; then
        echo "Erreur : La version spécifiée dans le commit ($VERSION) ne correspond pas à celle du fichier pom.xml."
        exit 1
    fi

    # Vérification du titre h2 dans CHANGELOG.md
    if ! grep -q "##.*$VERSION" CHANGELOG.md; then
        echo "Erreur : Le fichier CHANGELOG.md doit contenir un titre h2 (##) avec le numéro de version $VERSION."
        exit 1
    fi

    echo "Commit de montée de version détecté et validé : $COMMIT_MSG"
    exit 0
fi

if [[ ! $COMMIT_MSG =~ $REGEX ]]; then
    echo "Erreur : Le message de commit doit respecter la norme Conventional Commit."
    echo "Types autorisés : feat, fix, docs, style, refactor, test, chore, hotfix, ci."
    echo "Pour les commits de montée de version 'bump', utilisez : bump: vX.Y.Z ou vX.Y.Z-AAAAA"
    echo "'$COMMIT_MSG' n'est pas un message de commit valide."
    exit 1
fi

exit 0
