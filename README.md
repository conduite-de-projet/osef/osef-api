# osef-api

## Installation
1. Install Docker on your system (Docker Desktop/Engine or Orbstack for MacOS)
2. Run the following command:
   ```shell
   cp .env.example .env
   ````
3. Fill the .env file for 
   1. Database credentials (you can put mostly whatever you want)
   2. Ports, if the proposed range is not available on your machine you can update the exposed ports, do not change the
   "\_SERVICE\_" ports keys, they are specific to the service they're tagged with.
   3. DO NOT COMMIT the .env file.
4. Install [lefthook](https://github.com/evilmartians/lefthook/tree/master) on your machine for commit validation
5. Run 
```shell
lefthook install
```
To sync the hooks with your project
   1. You can see the config in `.lefthook/commit-msg/pre-commit.sh`, the script checks that the commit respects the
   conventional commit and the SEMVER.
5. Run the project with docker, you can check the Dockerfile to see that the build phase will automatically compile and
   build the maven project and create the `.jar` of the application
   ```shell
   docker compose up --build
   ````
You should see something like:
```docker
[+] Running 3/3
✔ Container osef-db       Running                                                                                                                                                                                          0.0s
✔ Container osef-api      Running                                                                                                                                                                                          0.0s
✔ Container osef-adminer  Running
```
When the process is done, check that everything is okay with:
```shell
docker ps | grep osef
````
```docker
f1dcac30a20d   adminer                        "entrypoint.sh php -…"   3 seconds ago   Up 2 seconds (health: starting)   0.0.0.0:9082->8080/tcp, [::]:9082->8080/tcp                                                    osef-adminer
9acbdc35d9bc   osef-api-api                   "java -jar /app/app.…"   3 seconds ago   Up 2 seconds (health: starting)   0.0.0.0:9080->8080/tcp, [::]:9080->8080/tcp                                                    osef-api
ba1030768403   postgres:14                    "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds (health: starting)   0.0.0.0:9081->5432/tcp, [::]:9081->5432/tcp                                                    osef-db
```
Check that the API is up by going to `http://localhost:9080/users`

## Usage
Once the application is up-and-running you can simply hit its endpoints to manipulate the API.
### Access the database
You can use your own RDBMS or use the `adminer` service provided in the stack, it's quite ugly but does the job to quickly check data, and it's directly accessible from your web browser.  
To connect the database from an RDBMS use `jdbc:postgresql://localhost:9081/osefdb` and fill the credentials provided in the .env file.


### Creating tables
A table is related to a model (in the com.osef.models package), a table is always plural.
Thanks to hibernate and flywaydb we can and will automatically generate the database and the migrations.
You don't have to create the table manually, when creating a model if you provided proper annotations hibernate will generate a DDL SQL script automatically on startup of the app and update the db.
So when adding a model don't forget to run 
```shell
docker-compose up --build api --force-recreate
```
Since it is a JAVA application we need to recompile the code after each modification.

See the User model for an example and the package documentation: https://javadoc.io/doc/jakarta.persistence/jakarta.persistence-api/latest/jakarta.persistence/jakarta/persistence/package-summary.html

## Create a model
To create with each new model you should create:
- The model -> com.osef.models
- A DTO for the model -> com.osef.api.v1.models.dto
  - Only create a DTO if you think the model might need internal manipulation. The DTO is here to prevent accidental manipulations on a database entity.
- A Repository -> com.osef.api.v1.repositories
- A Service -> com.osef.api.v1.services
  - Only create the service if the model should have routes or complex manipulation on it.
- A Controller -> com.osef.api.v1.controllers
  - Only create the controller if the model should have routes.
- A Seeder -> com.osef.db.seeders
  - In the seeder don't forget to add the `@PostConstruct` annotation, this way when you build the `.jar` the seeder will be played.

## Play with the API
One the app is up you can find a swagger at http://localhost:9080/swagger-ui/index.html. The swagger is auto-generated and based on the model declaration.