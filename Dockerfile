# JAR
FROM maven:3.9.4-eclipse-temurin-21-alpine AS container-builder

WORKDIR /app

COPY pom.xml ./
RUN mvn dependency:go-offline

COPY src ./src

RUN mvn clean package -DskipTests

# API
FROM openjdk:21-jdk-slim AS container

RUN apt-get update && apt-get install -y curl

WORKDIR /app

COPY --from=container-builder /app/target/osef-0.3.0.jar /app/app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
