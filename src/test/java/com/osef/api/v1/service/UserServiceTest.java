package com.osef.api.v1.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.repositories.UserRepository;
import com.osef.api.v1.services.UserService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

  @TestConfiguration
  static class TestConfig {
    @Bean
    public UserService userService() {
      return new UserService();
    }
  }

  @Autowired UserService userService;
  @MockBean UserRepository userRepository;

  @BeforeEach
  public void setUp() {
    User user =
        new User(
            "Prénom",
            "Nom",
            "email@mail.com",
            "passe".getBytes(),
            "".getBytes(),
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "Status");

    Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
    Mockito.when(userRepository.findAll()).thenReturn(java.util.List.of(user));
  }

  @Test
  public void testCreate() {
    UserDTO userDTO =
        new UserDTO(
            UUID.randomUUID(),
            "Prénom",
            "Nom",
            "email@mail.com",
            "passe",
            "Status",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            null,
            null,
            null);

    UserDTO user = userService.createUser(userDTO);
    assertTrue(user != null);
  }

  @Test
  public void testGetAll() {
    assertNotNull(userService.getAllUsers() != null);
  }
}
