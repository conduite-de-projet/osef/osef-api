package com.osef.api.v1.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.osef.api.v1.enums.TicketPriority;
import com.osef.api.v1.enums.TicketStatus;
import com.osef.api.v1.models.Ticket;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.repositories.TicketRepository;
import com.osef.api.v1.services.TicketService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class TicketServiceTest {

  @TestConfiguration
  static class TestConfig {
    @Bean
    public TicketService ticketService() {
      return new TicketService();
    }
  }

  @Autowired TicketService ticketService;

  @MockBean TicketRepository ticketRepository;

  @BeforeEach
  public void setUp() {
    Ticket ticket =
        new Ticket(
            UUID.randomUUID(),
            "Type",
            TicketStatus.TODO,
            "Title",
            "Description",
            TicketPriority.MEDIUM,
            2,
            0,
            null,
            null,
            null);

    Mockito.when(ticketRepository.save(Mockito.any())).thenReturn(ticket);
    Mockito.when(ticketRepository.findAll()).thenReturn(java.util.List.of(ticket));
  }

  @Test
  public void testCreate() {
    TicketDTO ticketDTO =
        new TicketDTO(
            UUID.randomUUID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            "Code",
            0,
            "Type",
            TicketStatus.TODO,
            "Title",
            "Description",
            TicketPriority.MEDIUM,
            2,
            0,
            null,
            null,
            null,
            null);

    TicketDTO ticket = ticketService.createTicket(ticketDTO);
    assertNotNull(ticket != null);
  }

  @Test
  public void testGetAll() {
    assertNotNull(ticketService.getAllTickets() != null);
  }
}
