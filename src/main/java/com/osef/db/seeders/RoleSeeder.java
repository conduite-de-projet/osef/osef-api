package com.osef.db.seeders;

import com.osef.api.v1.models.dto.RoleDTO;
import com.osef.api.v1.services.RoleService;
import jakarta.annotation.PostConstruct;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RoleSeeder {

  @Autowired private RoleService roleService;

  @PostConstruct
  public void seedRoles() {
    if (roleService.getAllRoles().isEmpty()) {
      System.out.println("Seeding roles...");
      roleService.createRole(new RoleDTO(UUID.randomUUID(), "user", "Default user", 0));
      roleService.createRole(new RoleDTO(UUID.randomUUID(), "admin", "Admin user", 1));
      System.out.println("Seeded roles successfully.");
    } else {
      System.out.println("Roles already exist, no seeding required.");
    }
  }
}
