package com.osef.db.seeders;

import com.osef.api.v1.models.dto.LabelDTO;
import com.osef.api.v1.services.LabelService;
import jakarta.annotation.PostConstruct;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("labelSeeder")
public class LabelSeeder {

  @Autowired private LabelService labelService;

  @PostConstruct
  public void seedLabels() {
    if (labelService.getLabels().isEmpty()) {
      System.out.println("Seeding labels...");
      labelService.createLabel(new LabelDTO(UUID.randomUUID(), "Bug", "#FFC4C4"));
      labelService.createLabel(new LabelDTO(UUID.randomUUID(), "Feature", "#A4D4FF"));
      labelService.createLabel(new LabelDTO(UUID.randomUUID(), "Enhancement", "#A7F3D0"));
      System.out.println("Seeded labels successfully.");
    } else {
      System.out.println("Labels already exist, no seeding required.");
    }
  }
}
