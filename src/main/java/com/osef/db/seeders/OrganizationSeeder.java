package com.osef.db.seeders;

import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.services.OrganizationService;
import jakarta.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrganizationSeeder {

  @Autowired private OrganizationService organizationService;

  @PostConstruct
  public void seedOrganizations() {
    if (organizationService.getAllOrganizations().isEmpty()) {
      System.out.println("Seeding organizations...");
      List<OrganizationDTO> organizations = generateOrganizations();
      organizations.forEach(organizationService::createOrganization);
      System.out.println("Seeded 3 organizations successfully.");
    } else {
      System.out.println("Organizations already exist, no seeding required.");
    }
  }

  private List<OrganizationDTO> generateOrganizations() {
    return IntStream.range(0, 3)
        .mapToObj(
            i ->
                new OrganizationDTO(
                    UUID.randomUUID(),
                    "organization_" + (i + 1),
                    "organization_" + (i + 1) + " description",
                    Timestamp.from(java.time.Instant.now()),
                    null))
        .collect(Collectors.toList());
  }
}
