package com.osef.db.seeders;

import com.osef.api.v1.models.Label;
import com.osef.api.v1.models.Ticket;
import com.osef.api.v1.models.TicketLabel;
import com.osef.api.v1.models.TicketLabelKey;
import com.osef.api.v1.repositories.LabelRepository;
import com.osef.api.v1.repositories.TicketLabelRepository;
import com.osef.api.v1.repositories.TicketRepository;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn({"ticketSeeder", "labelSeeder"})
public class TicketLabelSeeder implements ApplicationRunner {

  @Autowired private TicketRepository ticketRepository;
  @Autowired private LabelRepository labelRepository;
  @Autowired private TicketLabelRepository ticketLabelRepository;

  private final Random random = new Random();

  @Override
  @Transactional
  public void run(ApplicationArguments args) {
    List<Ticket> tickets = ticketRepository.findAll();
    List<Label> labels = labelRepository.findAll();

    if (!ticketLabelRepository.findAll().isEmpty()) {
      return;
    }

    if (tickets.isEmpty()) {
      System.out.println("No tickets found. Skipping ticket-label seeding.");
      return;
    }

    if (labels.isEmpty()) {
      System.out.println("No labels found. Skipping ticket-label seeding.");
      return;
    }

    tickets.forEach(
        ticket -> {
          if (random.nextBoolean()) {
            int numLabels = random.nextInt(labels.size()) + 1;
            List<Label> selectedLabels = labels.subList(0, numLabels);
            selectedLabels.forEach(
                label -> {
                  TicketLabel ticketLabel =
                      new TicketLabel(new TicketLabelKey(ticket.getId(), label.getId()));
                  ticketLabel.setTicket(ticket);
                  ticketLabel.setLabel(label);
                  ticketLabelRepository.save(ticketLabel);
                });
          }
        });

    System.out.println("Ticket-label seeding completed.");
  }
}
