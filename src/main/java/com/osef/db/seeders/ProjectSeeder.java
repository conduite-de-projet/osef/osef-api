package com.osef.db.seeders;

import com.osef.api.v1.enums.ProjectStatus;
import com.osef.api.v1.enums.ProjectVisibility;
import com.osef.api.v1.models.Organization;
import com.osef.api.v1.models.dto.ProjectDTO;
import com.osef.api.v1.models.dto.ProjectIconDTO;
import com.osef.api.v1.repositories.OrganizationRepository;
import com.osef.api.v1.services.ProjectService;
import jakarta.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component("projectSeeder")
public class ProjectSeeder implements ApplicationRunner {

  @Autowired private ProjectService projectService;
  @Autowired private OrganizationRepository organizationRepository;

  @Override
  @Transactional
  public void run(ApplicationArguments args) {
    if (projectService.getAllProjects().isEmpty()) {
      List<ProjectDTO> projects = generateProjects();
      projects.forEach(
          (project) -> {
            Organization organization = organizationRepository.findFirst();
            Hibernate.initialize(organization);
            System.out.println(organization);
            projectService.createProject(project, organization.getId());
          });
      System.out.println("Seeded 5 projects successfully.");
    } else {
      System.out.println("Projects already exist, no seeding required.");
    }
  }

  private List<ProjectDTO> generateProjects() {
    return IntStream.range(0, 5)
        .mapToObj(
            i ->
                new ProjectDTO(
                    UUID.randomUUID(),
                    "project_" + (i + 1),
                    "description_" + (i + 1),
                    ProjectStatus.ACTIVE,
                    Timestamp.from(java.time.Instant.now()),
                    Timestamp.from(java.time.Instant.now()),
                    ProjectVisibility.PUBLIC,
                    Timestamp.from(java.time.Instant.now()),
                    Timestamp.from(java.time.Instant.now()),
                    new ProjectIconDTO("CubeTransparent", "#F87171")))
        .collect(Collectors.toList());
  }
}
