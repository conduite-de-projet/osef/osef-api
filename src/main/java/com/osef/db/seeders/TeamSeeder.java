package com.osef.db.seeders;

import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.services.TeamService;
import jakarta.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TeamSeeder {

  @Autowired private TeamService teamService;

  @PostConstruct
  public void seedTeams() {
    if (teamService.getAllTeams().isEmpty()) {
      List<TeamDTO> teams = generateTeams();
      teams.forEach(teamService::createTeam);
      System.out.println("Seeded 5 teams successfully.");
    } else {
      System.out.println("Teams already exist, no seeding required.");
    }
  }

  private List<TeamDTO> generateTeams() {
    return IntStream.range(0, 5)
        .mapToObj(
            i ->
                new TeamDTO(
                    UUID.randomUUID(), "team_" + (i + 1), "description_" + (i + 1), "", null, null))
        .collect(Collectors.toList());
  }
}
