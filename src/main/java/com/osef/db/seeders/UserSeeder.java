package com.osef.db.seeders;

import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.models.enums.ProfileColors;
import com.osef.api.v1.services.UserService;
import jakarta.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserSeeder {

  @Autowired private UserService userService;

  @PostConstruct
  public void seedUsers() {
    if (userService.getAllUsers().isEmpty()) {
      List<UserDTO> users = generateUsers();
      users.forEach(userService::createUser);
      System.out.println("Seeded 10 users successfully.");
    } else {
      System.out.println("Users already exist, no seeding required.");
    }
  }

  private List<UserDTO> generateUsers() {
    return IntStream.range(0, 10)
        .mapToObj(
            i ->
                new UserDTO(
                    UUID.randomUUID(),
                    "user_" + (i + 1),
                    "user_" + (i + 1),
                    "user_" + (i + 1) + "@osef.com",
                    "password",
                    ProfileColors.values()[Math.min(i, (ProfileColors.values().length - 1))]
                        .toString()
                        .toLowerCase(),
                    "Bordeaux",
                    "33300",
                    "address",
                    "France",
                    "my bio",
                    "https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-api/",
                    "active",
                    null,
                    null,
                    null))
        .collect(Collectors.toList());
  }
}
