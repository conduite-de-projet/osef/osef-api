package com.osef.db.seeders;

import com.osef.api.v1.enums.TicketPriority;
import com.osef.api.v1.enums.TicketStatus;
import com.osef.api.v1.helpers.EnumUtils;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.repositories.ProjectRepository;
import com.osef.api.v1.repositories.TicketRepository;
import com.osef.api.v1.repositories.UserRepository;
import com.osef.api.v1.services.TicketService;
import jakarta.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component("ticketSeeder")
@DependsOn({"projectSeeder"})
public class TicketSeeder implements ApplicationRunner {
  @Autowired private TicketService ticketService;
  @Autowired private ProjectRepository projectRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private TicketRepository ticketRepository;

  @Override
  @Transactional
  public void run(ApplicationArguments args) {
    if (ticketService.getAllTickets().isEmpty()) {
      List<TicketDTO> tickets = generateTickets();
      tickets.forEach(ticketService::createTicket);
      Project project = projectRepository.findFirst();
      if (project == null) {
        return;
      }
      User user = userRepository.findFirst();
      Hibernate.initialize(project);
      Hibernate.initialize(user);
      ticketRepository
          .findAll()
          .forEach(
              (ticket) -> {
                ticket.setProject(project);
                ticket.setAssignee(user);
                ticket.setAuthor(user);
                int reference =
                    TicketService.TicketCodeGenerator.calculateNextReference(
                        project.getTickets().stream().toList());
                String code =
                    TicketService.TicketCodeGenerator.generateTicketCode(project, reference);
                ticket.setCode(code);
                ticket.setReference(reference);
                project.getTickets().add(ticket);
                projectRepository.save(project);
              });
      System.out.println("Seeded 10 tickets successfully.");
    } else {
      System.out.println("Tickets already exist, no seeding required.");
    }
  }

  private List<TicketDTO> generateTickets() {
    return IntStream.range(0, 10)
        .mapToObj(
            i ->
                new TicketDTO(
                    UUID.randomUUID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    "task",
                    EnumUtils.getRandomEnumValue(TicketStatus.class),
                    "task_" + (i + 1),
                    "task_" + (i + 1),
                    EnumUtils.getRandomEnumValue(TicketPriority.class),
                    0,
                    0,
                    LocalDate.now(),
                    null,
                    Timestamp.from(Instant.now()),
                    Timestamp.from(Instant.now())))
        .collect(Collectors.toList());
  }
}
