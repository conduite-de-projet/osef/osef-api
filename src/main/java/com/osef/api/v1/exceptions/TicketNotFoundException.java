package com.osef.api.v1.exceptions;

import java.util.UUID;

public class TicketNotFoundException extends NotFoundException {
  public TicketNotFoundException(UUID id) {
    super("Ticket not found with id: " + id);
  }
}
