package com.osef.api.v1.exceptions;

public abstract class NotFoundException extends RuntimeException {

  public NotFoundException(String message) {
    super(message);
  }
}
