package com.osef.api.v1.exceptions;

import java.util.UUID;

public class TeamNotInOrganizationException extends NotFoundException {
  public TeamNotInOrganizationException(UUID teamId, UUID organizationId) {
    super("Team with id :" + teamId + " is not in organization with id : " + organizationId);
  }
}
