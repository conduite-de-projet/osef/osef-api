package com.osef.api.v1.exceptions;

import java.util.UUID;

public class OrganizationNotFoundException extends NotFoundException {

  public OrganizationNotFoundException(UUID id) {
    super("Organization with id " + id + " not found");
  }
}
