package com.osef.api.v1.exceptions;

import java.util.UUID;

public class RoleNotFoundException extends NotFoundException {
  public RoleNotFoundException(UUID roleId) {
    super("Role not found with id " + roleId);
  }
}
