package com.osef.api.v1.exceptions;

import java.util.UUID;

public class UserNotInOrganization extends NotFoundException {

  public UserNotInOrganization(UUID id) {
    super("User " + id + " is not part of an organization");
  }
}
