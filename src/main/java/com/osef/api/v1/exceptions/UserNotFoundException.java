package com.osef.api.v1.exceptions;

import java.util.UUID;

public class UserNotFoundException extends NotFoundException {

  public UserNotFoundException(UUID id) {
    super("User not found with id: " + id);
  }

  public UserNotFoundException(String email) {
    super("User not found with email: " + email);
  }
}
