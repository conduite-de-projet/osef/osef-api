package com.osef.api.v1.exceptions;

import java.util.UUID;

public class UserNotInTeamException extends NotFoundException {
  public UserNotInTeamException(UUID userId, UUID teamId) {
    super("User with id " + userId + " is not in team with id " + teamId);
  }
}
