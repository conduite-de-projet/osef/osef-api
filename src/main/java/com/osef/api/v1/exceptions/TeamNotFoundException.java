package com.osef.api.v1.exceptions;

import java.util.UUID;

public class TeamNotFoundException extends NotFoundException {

  public TeamNotFoundException(UUID id) {
    super("Team not found with id: " + id);
  }
}
