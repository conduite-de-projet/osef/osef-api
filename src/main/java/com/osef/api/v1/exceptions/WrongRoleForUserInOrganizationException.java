package com.osef.api.v1.exceptions;

import java.util.UUID;

public class WrongRoleForUserInOrganizationException extends NotFoundException {
  public WrongRoleForUserInOrganizationException(UUID userId, UUID roleId, UUID organizationId) {
    super(
        String.format(
            "User with id %s does not have role with id %s in organization with id %s",
            userId, roleId, organizationId));
  }
}
