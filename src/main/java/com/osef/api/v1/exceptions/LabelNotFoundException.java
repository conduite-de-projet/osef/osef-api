package com.osef.api.v1.exceptions;

import java.util.UUID;

public class LabelNotFoundException extends NotFoundException {

  public LabelNotFoundException(UUID id) {
    super("Label with id " + id + " not found");
  }
}
