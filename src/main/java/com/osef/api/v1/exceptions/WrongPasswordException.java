package com.osef.api.v1.exceptions;

public class WrongPasswordException extends RuntimeException {

  public WrongPasswordException() {
    super("Incorrect password");
  }
}
