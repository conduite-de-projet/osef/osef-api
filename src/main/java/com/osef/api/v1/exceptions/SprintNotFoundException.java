package com.osef.api.v1.exceptions;

import java.util.UUID;

public class SprintNotFoundException extends NotFoundException {
  public SprintNotFoundException(UUID id) {
    super("Sprint not found with id: " + id);
  }
}
