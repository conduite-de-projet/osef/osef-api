package com.osef.api.v1.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<String> handleNotFound(NotFoundException ex) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ex.getMessage());
  }

  @ExceptionHandler(WrongPasswordException.class)
  public ResponseEntity<String> handleWrongPassword(WrongPasswordException ex) {
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<String> handleGeneralException(Exception ex) {
    StringBuilder sb = new StringBuilder();
    sb.append("An unexpected error occurred : <br>");
    sb.append(ex.getMessage());

    for (StackTraceElement ste : ex.getStackTrace()) {
      sb.append(ste.toString());
      sb.append("<br>");
    }

    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(sb.toString());
  }
}
