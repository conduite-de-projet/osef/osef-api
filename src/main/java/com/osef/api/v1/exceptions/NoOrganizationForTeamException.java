package com.osef.api.v1.exceptions;

import java.util.UUID;

public class NoOrganizationForTeamException extends NotFoundException {

  public NoOrganizationForTeamException(UUID teamId) {
    super("Team with id: " + teamId + " has no organization");
  }
}
