package com.osef.api.v1.exceptions;

import java.util.UUID;

public class RepositoryNotFoundException extends RuntimeException {

  public RepositoryNotFoundException(UUID id) {
    super("Repository with id " + id + " not found");
  }
}
