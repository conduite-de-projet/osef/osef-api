package com.osef.api.v1.repositories;

import com.osef.api.v1.models.Repository;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RepositoryRepository
    extends JpaRepository<Repository, UUID>, JpaSpecificationExecutor<Repository> {}
