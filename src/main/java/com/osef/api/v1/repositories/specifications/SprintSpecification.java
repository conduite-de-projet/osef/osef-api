package com.osef.api.v1.repositories.specifications;

import com.osef.api.v1.enums.SprintStatus;
import com.osef.api.v1.models.Sprint;
import java.util.UUID;
import org.springframework.data.jpa.domain.Specification;

public class SprintSpecification {

  public static Specification<Sprint> hasName(String name) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.like(
            criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%");
  }

  public static Specification<Sprint> hasStatus(SprintStatus status) {
    return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("status"), status);
  }

  public static Specification<Sprint> hasProjectId(UUID projectId) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.get("project").get("id"), projectId);
  }
}
