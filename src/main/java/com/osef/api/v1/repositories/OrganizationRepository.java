package com.osef.api.v1.repositories;

import com.osef.api.v1.models.Organization;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface OrganizationRepository
    extends JpaRepository<Organization, UUID>, JpaSpecificationExecutor<Organization> {

  @Query(value = "SELECT * FROM organizations LIMIT 1", nativeQuery = true)
  Organization findFirst();
}
