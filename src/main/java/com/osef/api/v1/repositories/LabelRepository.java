package com.osef.api.v1.repositories;

import com.osef.api.v1.models.Label;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LabelRepository
    extends JpaRepository<Label, UUID>, JpaSpecificationExecutor<Label> {}
