package com.osef.api.v1.repositories.specifications;

import com.osef.api.v1.models.Ticket;
import jakarta.persistence.criteria.JoinType;
import java.time.LocalDate;
import java.util.UUID;
import org.springframework.data.jpa.domain.Specification;

public class TicketSpecification {

  public static Specification<Ticket> hasTitle(String title) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.like(
            criteriaBuilder.lower(root.get("title")), "%" + title.toLowerCase() + "%");
  }

  public static Specification<Ticket> hasDescription(String description) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.like(
            criteriaBuilder.lower(root.get("description")), "%" + description.toLowerCase() + "%");
  }

  public static Specification<Ticket> hasType(String type) {
    return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("type"), type);
  }

  public static Specification<Ticket> hasStatus(String status) {
    return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("status"), status);
  }

  public static Specification<Ticket> hasPriority(String priority) {
    return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("priority"), priority);
  }

  public static Specification<Ticket> hasParentTicket(UUID parentTicketId) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.get("parentTicketId"), parentTicketId);
  }

  public static Specification<Ticket> hasProject(UUID projectId) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.get("project").get("id"), projectId);
  }

  public static Specification<Ticket> hasSprint(UUID sprintId) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.get("sprint").get("id"), sprintId);
  }

  public static Specification<Ticket> hasDeadlineBefore(LocalDate deadline) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.lessThanOrEqualTo(root.get("deadline"), deadline);
  }

  public static Specification<Ticket> hasDeadlineAfter(LocalDate deadline) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.greaterThanOrEqualTo(root.get("deadline"), deadline);
  }

  public static Specification<Ticket> hasEstimatedTimeLessThan(Integer estimatedTime) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.lessThanOrEqualTo(root.get("estimatedTime"), estimatedTime);
  }

  public static Specification<Ticket> hasTimeSpentGreaterThan(Integer timeSpent) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.greaterThanOrEqualTo(root.get("timeSpent"), timeSpent);
  }

  public static Specification<Ticket> hasLabel(String labelName) {
    return (root, query, criteriaBuilder) -> {
      query.distinct(true);
      return criteriaBuilder.like(
          criteriaBuilder.lower(root.join("labels", JoinType.LEFT).get("name")),
          "%" + labelName.toLowerCase() + "%");
    };
  }

  public static Specification<Ticket> orderByCreatedAtDesc() {
    return (root, query, criteriaBuilder) -> {
      if (query.getResultType() != Long.class) {
        query.orderBy(criteriaBuilder.desc(root.get("createdAt")));
      }
      return null;
    };
  }
}
