package com.osef.api.v1.repositories;

import com.osef.api.v1.models.OrganizationUserRole;
import com.osef.api.v1.models.OrganizationUserRoleKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OrganizationUserRoleRepository
    extends JpaRepository<OrganizationUserRole, OrganizationUserRoleKey>,
        JpaSpecificationExecutor<OrganizationUserRole> {}
