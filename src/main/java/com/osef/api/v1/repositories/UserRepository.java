package com.osef.api.v1.repositories;

import com.osef.api.v1.models.User;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {
  @Query(value = "SELECT * FROM users LIMIT 1", nativeQuery = true)
  User findFirst();
}
