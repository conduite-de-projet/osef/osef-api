package com.osef.api.v1.repositories.specifications;

import com.osef.api.v1.models.User;
import org.springframework.data.jpa.domain.Specification;

public class UserSpecification {

  public static Specification<User> hasName(String name) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.like(root.get("name"), "%" + name + "%");
  }

  public static Specification<User> hasEmail(String email) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.like(root.get("email"), "%" + email + "%");
  }
}
