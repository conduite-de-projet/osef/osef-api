package com.osef.api.v1.repositories;

import com.osef.api.v1.models.Project;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface ProjectRepository
    extends JpaRepository<Project, UUID>, JpaSpecificationExecutor<Project> {
  @Query(value = "SELECT * FROM projects LIMIT 1", nativeQuery = true)
  Project findFirst();
}
