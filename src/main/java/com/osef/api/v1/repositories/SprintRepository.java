package com.osef.api.v1.repositories;

import com.osef.api.v1.enums.SprintStatus;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.Sprint;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SprintRepository
    extends JpaRepository<Sprint, UUID>, JpaSpecificationExecutor<Sprint> {
  @Query(
      "SELECT s "
          + "FROM Sprint s "
          + "WHERE s.project = :project "
          + "AND s.reference > :reference "
          + "AND s.status = :status "
          + "ORDER BY s.reference ASC"
          + " LIMIT 1")
  Sprint findFirstByProjectAndReferenceGreaterThanAndStatus(
      @Param("project") Project project,
      @Param("reference") Integer reference,
      @Param("status") SprintStatus status);
}
