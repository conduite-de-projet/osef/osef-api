package com.osef.api.v1.repositories;

import com.osef.api.v1.models.TeamUser;
import com.osef.api.v1.models.TeamUserKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TeamUserRepository
    extends JpaRepository<TeamUser, TeamUserKey>, JpaSpecificationExecutor<TeamUser> {}
