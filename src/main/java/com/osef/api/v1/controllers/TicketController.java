package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.LabelDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.services.TicketLabelService;
import com.osef.api.v1.services.TicketService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tickets")
public class TicketController {
  @Autowired private TicketService ticketService;
  @Autowired private TicketLabelService ticketLabelService;

  @Operation(summary = "Get all Tickets")
  @GetMapping
  public ResponseEntity<List<TicketDTO>> getTickets() {
    return ResponseEntity.ok(ticketService.getAllTickets());
  }

  @Operation(summary = "Get a Ticket")
  @GetMapping("/{id}")
  public ResponseEntity<TicketDTO> getTicketById(@PathVariable UUID id) {
    return ResponseEntity.ok(ticketService.getTicketById(id));
  }

  @Operation(summary = "Create a Ticket")
  @PostMapping
  public ResponseEntity<TicketDTO> createTicket(@RequestBody TicketDTO ticketDTO) {
    return ResponseEntity.ok(ticketService.createTicket(ticketDTO));
  }

  @Operation(summary = "Update a Ticket")
  @PutMapping("/{id}")
  public ResponseEntity<TicketDTO> updateTicket(
      @PathVariable UUID id, @RequestBody TicketDTO updatedTicket) {
    return ResponseEntity.ok(ticketService.updateTicket(id, updatedTicket));
  }

  @Operation(summary = "Delete a Ticket")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteTicket(@PathVariable UUID id) {
    ticketService.deleteTicket(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get all Labels from a Ticket")
  @GetMapping("/{id}/labels")
  public ResponseEntity<List<LabelDTO>> getLabelsFromTicket(@PathVariable UUID id) {
    return ResponseEntity.ok(ticketLabelService.getLabelsFromTicket(id));
  }

  @Operation(summary = "Add a Label to a Ticket")
  @PostMapping("/{ticketId}/labels")
  public ResponseEntity<TicketDTO> addLabelToTicket(
      @PathVariable UUID ticketId, @RequestBody UUID labelId) {
    return ResponseEntity.ok(ticketLabelService.addLabelToTocket(ticketId, labelId));
  }

  @Operation(summary = "Remove a Label from a Ticket")
  @DeleteMapping("/{ticketId}/labels/{labelId}")
  public ResponseEntity<Void> removeLabelFromTicket(
      @PathVariable UUID ticketId, @PathVariable UUID labelId) {
    ticketLabelService.removeLabelFromTicket(ticketId, labelId);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get ticket assignee")
  @GetMapping("/{id}/assignee")
  public ResponseEntity<UserDTO> getTicketAssignee(@PathVariable UUID id) {
    return ResponseEntity.ok(ticketService.getAssignee(id));
  }

  @Operation(summary = "Get ticket author")
  @GetMapping("/{id}/author")
  public ResponseEntity<UserDTO> getTicketAuthor(@PathVariable UUID id) {
    return ResponseEntity.ok(ticketService.getAuthor(id));
  }
}
