package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.RoleDTO;
import com.osef.api.v1.services.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
public class RoleController {
  @Autowired private RoleService roleService;

  @Operation(summary = "List all roles")
  @GetMapping
  public List<RoleDTO> getRoles() {
    return roleService.getAllRoles();
  }
}
