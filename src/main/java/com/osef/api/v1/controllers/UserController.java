package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.LoginDTO;
import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

  @Autowired private UserService userService;

  @Operation(summary = "Create an User")
  @PostMapping
  public UserDTO createUser(@RequestBody UserDTO user) {
    return userService.createUser(user);
  }

  @Operation(summary = "Get an User")
  @GetMapping("/{id}")
  public ResponseEntity<UserDTO> getUserById(@PathVariable UUID id) {
    return ResponseEntity.ok(userService.getUserById(id));
  }

  @Operation(summary = "Get all Users")
  @GetMapping
  public List<UserDTO> getUsers(
      @RequestParam(required = false) String name, @RequestParam(required = false) String email) {
    return userService.filterUsers(name, email);
  }

  @Operation(summary = "Update an User")
  @PutMapping("/{id}")
  public ResponseEntity<UserDTO> updateUser(
      @PathVariable UUID id, @RequestBody UserDTO updatedUser) {
    return ResponseEntity.ok(userService.updateUser(id, updatedUser));
  }

  @Operation(summary = "Delete an User")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteUser(@PathVariable UUID id) {
    userService.deleteUser(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get organization of a user")
  @GetMapping("/{id}/organization")
  public ResponseEntity<OrganizationDTO> getUserOrganization(@PathVariable UUID id) {
    return ResponseEntity.ok(userService.getUserOrganization(id));
  }

  @Operation(summary = "Login")
  @PostMapping("/login")
  public ResponseEntity<UserDTO> login(@RequestBody LoginDTO user) {
    return ResponseEntity.ok(userService.login(user));
  }
}
