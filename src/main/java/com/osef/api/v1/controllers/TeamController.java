package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.services.TeamService;
import com.osef.api.v1.services.TeamUserService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teams")
public class TeamController {

  @Autowired private TeamService teamService;

  @Autowired private TeamUserService teamUserService;

  @Operation(summary = "Get all Teams")
  @GetMapping
  public List<TeamDTO> getTeams() {
    return teamService.getAllTeams();
  }

  @Operation(summary = "Create a Team")
  @PostMapping
  public TeamDTO createTeam(@RequestBody TeamDTO team) {
    return teamService.createTeam(team);
  }

  @Operation(summary = "Get a Team")
  @GetMapping("/{id}")
  public ResponseEntity<TeamDTO> getTeamById(@PathVariable UUID id) {
    return ResponseEntity.ok(teamService.getTeamById(id));
  }

  @Operation(summary = "Update a Team")
  @PutMapping("/{id}")
  public ResponseEntity<TeamDTO> updateTeam(
      @PathVariable UUID id, @RequestBody TeamDTO updatedTeam) {
    return ResponseEntity.ok(teamService.updateTeam(id, updatedTeam));
  }

  @Operation(summary = "Delete a Team")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteTeam(@PathVariable UUID id) {
    teamService.deleteTeam(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Gets all users from a team")
  @GetMapping("/{teamId}/users")
  public ResponseEntity<List<UserDTO>> getUsersInTeam(@PathVariable UUID teamId) {
    return ResponseEntity.ok(teamUserService.getUsersFromTeam(teamId));
  }

  @Operation(summary = "Add a User to a Team")
  @PutMapping("/{teamId}/users")
  public ResponseEntity<TeamDTO> addUserToTeam(
      @PathVariable UUID teamId, @RequestBody UUID userId) {
    return ResponseEntity.ok(teamUserService.addUserToTeam(teamId, userId));
  }

  @Operation(summary = "Remove a User from a Team")
  @DeleteMapping("/{teamId}/users/{userId}")
  public ResponseEntity<Void> removeUserFromTeam(
      @PathVariable UUID teamId, @PathVariable UUID userId) {
    teamUserService.removeUserFromTeam(teamId, userId);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get the organization of a Team")
  @GetMapping("/{teamId}/organization")
  public ResponseEntity<OrganizationDTO> getOrganizationFromTeam(@PathVariable UUID teamId) {
    return ResponseEntity.ok(teamService.getOrganization(teamId));
  }
}
