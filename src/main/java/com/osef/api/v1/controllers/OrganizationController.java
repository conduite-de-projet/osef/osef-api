package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.models.dto.UserRoleDto;
import com.osef.api.v1.services.OrganizationService;
import com.osef.api.v1.services.OrganizationUserRoleService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/organizations")
public class OrganizationController {
  @Autowired private OrganizationService organizationService;

  @Autowired private OrganizationUserRoleService organizationUserRoleService;

  @Operation(summary = "Create an Organization")
  @PostMapping
  public OrganizationDTO createOrganization(@RequestBody OrganizationDTO organization) {
    return organizationService.createOrganization(organization);
  }

  @Operation(summary = "Get an Organization")
  @GetMapping("/{id}")
  public ResponseEntity<OrganizationDTO> getOrganizationById(@PathVariable UUID id) {
    return ResponseEntity.ok(organizationService.getOrganizationById(id));
  }

  @Operation(summary = "Get all Organizations")
  @GetMapping
  public List<OrganizationDTO> getOrganizations() {
    return organizationService.getAllOrganizations();
  }

  @Operation(summary = "Update an Organization")
  @PutMapping("/{id}")
  public ResponseEntity<OrganizationDTO> updateOrganization(
      @PathVariable UUID id, @RequestBody OrganizationDTO updatedOrganization) {
    return ResponseEntity.ok(organizationService.updateOrganization(id, updatedOrganization));
  }

  @Operation(summary = "Delete an Organization")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteOrganization(@PathVariable UUID id) {
    organizationService.deleteOrganization(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Lists all Team from an Organization")
  @GetMapping("/{organizationId}/teams")
  public ResponseEntity<List<TeamDTO>> getTeamFromOrganization(@PathVariable UUID organizationId) {
    return ResponseEntity.ok(organizationService.getTeamsFromOrganization(organizationId));
  }

  @Operation(summary = "Add a Team to an Organization")
  @PutMapping("/{organizationId}/teams")
  public ResponseEntity<OrganizationDTO> addTeamToOrganization(
      @PathVariable UUID organizationId, @RequestBody UUID teamId) {
    return ResponseEntity.ok(organizationService.addTeamToOrganization(organizationId, teamId));
  }

  @Operation(summary = "Remove a Team from an Organization")
  @DeleteMapping("/{organizationId}/teams/{teamId}")
  public ResponseEntity<Void> removeTeamFromOrganization(
      @PathVariable UUID organizationId, @PathVariable UUID teamId) {
    organizationService.removeTeamFromOrganization(organizationId, teamId);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Lists all User Roles from an Organization")
  @GetMapping("/{organizationId}/roles")
  public ResponseEntity<List<UserRoleDto>> getUserRoles(@PathVariable UUID organizationId) {
    return ResponseEntity.ok(
        organizationUserRoleService.getUserRolesFromOrganization(organizationId));
  }

  @Operation(summary = "Add a Role to a user in an Organization")
  @PutMapping("/{organizationId}/roles")
  public ResponseEntity<UserRoleDto> addUserRoleToOrganization(
      @PathVariable UUID organizationId, @RequestBody UserRoleDto userRole) {
    return ResponseEntity.ok(
        organizationUserRoleService.addUserRoleToOrganization(
            organizationId, userRole.user().id(), userRole.role().id()));
  }

  @Operation(summary = "Remove a Role from a user in an Organization")
  @DeleteMapping("/{organizationId}/roles/{userId}/{roleId}")
  public ResponseEntity<Void> removeUserRoleFromOrganization(
      @PathVariable UUID organizationId, @PathVariable UUID userId, @PathVariable UUID roleId) {
    organizationUserRoleService.deleteUserRoleFromOrganization(organizationId, userId, roleId);
    return ResponseEntity.noContent().build();
  }
}
