package com.osef.api.v1.controllers;

import com.osef.api.v1.enums.SprintStatus;
import com.osef.api.v1.models.dto.ProjectDTO;
import com.osef.api.v1.models.dto.ProjectRepositoryDTO;
import com.osef.api.v1.models.dto.RepositoryDTO;
import com.osef.api.v1.models.dto.SprintDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.services.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectController {

  @Autowired private ProjectService projectService;

  @Operation(summary = "Get all Projects")
  @GetMapping
  public List<ProjectDTO> getProjects() {
    return projectService.getAllProjects();
  }

  @Operation(summary = "Create a Project")
  @PostMapping
  public ProjectDTO createProject(@RequestBody ProjectDTO project) {
    return projectService.createProject(project);
  }

  @Operation(summary = "Get a Project")
  @GetMapping("/{id}")
  public ResponseEntity<ProjectDTO> getProjectById(@PathVariable UUID id) {
    return ResponseEntity.ok(projectService.getProjectById(id));
  }

  @Operation(summary = "Update a Project")
  @PutMapping("/{id}")
  public ResponseEntity<ProjectDTO> updateProject(
      @PathVariable UUID id, @RequestBody ProjectDTO updatedProject) {
    return ResponseEntity.ok(projectService.updateProject(id, updatedProject));
  }

  @Operation(summary = "Delete a Project")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteProject(@PathVariable UUID id) {
    projectService.deleteProject(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Create a Repository for a Project")
  @PostMapping("/{id}/repositories")
  public ResponseEntity<ProjectRepositoryDTO> createProjectRepository(
      @PathVariable UUID id, @RequestBody RepositoryDTO repository) {
    return ResponseEntity.ok(projectService.createRepository(id, repository));
  }

  @Operation(summary = "Get Project Repositories")
  @GetMapping("/{id}/repositories")
  public ResponseEntity<List<RepositoryDTO>> getProjectRepositories(@PathVariable UUID id) {
    return ResponseEntity.ok(projectService.getRepositories(id));
  }

  @Operation(summary = "Create a Ticket for a Project")
  @PostMapping("/{id}/tickets")
  public ResponseEntity<TicketDTO> createProjectTicket(
      @PathVariable UUID id, @RequestBody TicketDTO ticket) {
    return ResponseEntity.ok(projectService.createTicket(id, ticket));
  }

  @Operation(summary = "Create a Ticket for a Project")
  @PostMapping("/{id}/repository/{repositoryId}/tickets/{remoteId}")
  public ResponseEntity<TicketDTO> createProjectRepositoryTicket(
      @PathVariable UUID id,
      @PathVariable UUID repositoryId,
      @PathVariable Integer remoteId,
      @RequestBody TicketDTO ticket) {
    return ResponseEntity.ok(
        projectService.createRepositoryTicket(id, ticket, repositoryId, remoteId));
  }

  @Operation(summary = "Get Project Tickets")
  @GetMapping("/{id}/tickets")
  public ResponseEntity<List<TicketDTO>> getProjectTickets(@PathVariable UUID id) {
    return ResponseEntity.ok(projectService.getTickets(id));
  }

  @Operation(summary = "Get Project last week done tickets")
  @GetMapping("/{id}/tickets/-/done-last-two-weeks")
  public ResponseEntity<Map<String, List<TicketDTO>>> getProjectTicketsDoneLastWeek(
      @PathVariable UUID id) {
    return ResponseEntity.ok(projectService.getTicketsDoneLastTwoWeeks(id));
  }

  @Operation(summary = "Get Project Sprints")
  @PostMapping("/{id}/sprints")
  public ResponseEntity<SprintDTO> createProjectSprints(
      @PathVariable UUID id, @RequestBody SprintDTO sprint) {
    return ResponseEntity.ok(projectService.createSprint(id, sprint));
  }

  @Operation(summary = "Get Project Sprints")
  @GetMapping("/{id}/sprints")
  public ResponseEntity<List<SprintDTO>> getProjectSprints(
      @PathVariable UUID id, @RequestParam(required = false) SprintStatus status) {
    return ResponseEntity.ok(projectService.getSprints(id, status));
  }

  @Operation(summary = "Get Project active Sprint")
  @GetMapping("/{id}/sprints/active")
  public ResponseEntity<SprintDTO> getProjectActiveSprint(@PathVariable UUID id) {
    Optional<SprintDTO> sprint =
        projectService.getSprints(id, SprintStatus.IN_PROGRESS).stream().findFirst();
    return sprint.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
  }
}
