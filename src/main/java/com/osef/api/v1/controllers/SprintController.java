package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.SprintDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.services.SprintService;
import com.osef.api.v1.services.TicketService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sprints")
public class SprintController {
  @Autowired private TicketService ticketService;
  @Autowired private SprintService sprintService;

  @Operation(summary = "Get all sprints")
  @GetMapping
  public ResponseEntity<List<SprintDTO>> getSprints() {
    return ResponseEntity.ok(sprintService.getAllSprints());
  }

  @Operation(summary = "Get a Sprint")
  @GetMapping("/{id}")
  public ResponseEntity<SprintDTO> getSprintById(@PathVariable UUID id) {
    return ResponseEntity.ok(sprintService.getSprintById(id));
  }

  @Operation(summary = "Create a Sprint")
  @PostMapping
  public ResponseEntity<SprintDTO> createSprint(@RequestBody SprintDTO sprintDTO) {
    return ResponseEntity.ok(sprintService.createSprint(sprintDTO));
  }

  @Operation(summary = "Update a Sprint")
  @PutMapping("/{id}")
  public ResponseEntity<SprintDTO> updateSprint(
      @PathVariable UUID id, @RequestBody SprintDTO updatedSprint) {
    return ResponseEntity.ok(sprintService.updateSprint(id, updatedSprint));
  }

  @Operation(summary = "Delete a Sprint")
  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteSprint(@PathVariable UUID id) {
    sprintService.deleteSprint(id);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get all Tickets from a Sprint")
  @GetMapping("/{id}/tickets")
  public ResponseEntity<List<TicketDTO>> getSprintTickets(@PathVariable UUID id) {
    return ResponseEntity.ok(sprintService.getSprintTickets(id));
  }

  @Operation(summary = "Create a Ticket for Sprint")
  @PostMapping("/{id}/tickets")
  public ResponseEntity<SprintDTO> createTicketForSprint(
      @PathVariable UUID id, @RequestBody TicketDTO ticketDTO) {
    return ResponseEntity.ok(sprintService.createTicket(id, ticketDTO));
  }

  @Operation(summary = "Add a Ticket to a Sprint")
  @PutMapping("/{id}/tickets/{ticketId}")
  public ResponseEntity<SprintDTO> addTicketToSprint(
      @PathVariable UUID id, @PathVariable UUID ticketId) {
    return ResponseEntity.ok(sprintService.addTicket(id, ticketId));
  }

  @Operation(summary = "Remove a Ticket from a Sprint")
  @DeleteMapping("/{id}/ticket/{ticketId}")
  public ResponseEntity<Void> removeLabelFromTicket(
      @PathVariable UUID id, @PathVariable UUID ticketId) {
    sprintService.removeTicket(ticketId, ticketId);
    return ResponseEntity.noContent().build();
  }

  @Operation(summary = "Get Sprint last week done tickets")
  @GetMapping("/{id}/tickets/-/done-last-two-weeks")
  public ResponseEntity<Map<String, List<TicketDTO>>> getProjectTicketsDoneLastWeek(
      @PathVariable UUID id) {
    return ResponseEntity.ok(sprintService.getTicketsDoneLastTwoWeeks(id));
  }

  @Operation(summary = "Start a Sprint")
  @PostMapping("/{id}/-/start")
  public ResponseEntity<SprintDTO> startSprint(@PathVariable UUID id) {
    return ResponseEntity.ok(sprintService.startSprint(id));
  }

  @Operation(summary = "Terminate a Sprint")
  @PostMapping("/{id}/-/terminate")
  public ResponseEntity<SprintDTO> terminateSprint(@PathVariable UUID id) {
    return ResponseEntity.ok(sprintService.terminateSprint(id));
  }
}
