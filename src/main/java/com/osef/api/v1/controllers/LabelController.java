package com.osef.api.v1.controllers;

import com.osef.api.v1.models.dto.LabelDTO;
import com.osef.api.v1.services.LabelService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/labels")
public class LabelController {

  @Autowired private LabelService labelService;

  @Operation(summary = "Get all Labels")
  @GetMapping
  public ResponseEntity<List<LabelDTO>> getLabels() {
    return ResponseEntity.ok(labelService.getLabels());
  }
}
