package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.OrganizationNotFoundException;
import com.osef.api.v1.exceptions.RoleNotFoundException;
import com.osef.api.v1.exceptions.UserNotFoundException;
import com.osef.api.v1.exceptions.WrongRoleForUserInOrganizationException;
import com.osef.api.v1.models.Organization;
import com.osef.api.v1.models.OrganizationUserRole;
import com.osef.api.v1.models.OrganizationUserRoleKey;
import com.osef.api.v1.models.Role;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.RoleDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.models.dto.UserRoleDto;
import com.osef.api.v1.repositories.OrganizationRepository;
import com.osef.api.v1.repositories.OrganizationUserRoleRepository;
import com.osef.api.v1.repositories.RoleRepository;
import com.osef.api.v1.repositories.UserRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationUserRoleService {

  @Autowired private OrganizationUserRoleRepository ourRepository;

  @Autowired private OrganizationRepository organizationRepository;

  @Autowired private RoleRepository roleRepository;

  @Autowired private UserRepository userRepository;

  public List<UserRoleDto> getUserRolesFromOrganization(UUID organizationId) {
    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(() -> new OrganizationNotFoundException(organizationId));
    return organization.getRoles().stream().map(our -> convertToDTO(our)).toList();
  }

  public UserRoleDto addUserRoleToOrganization(UUID organizationId, UUID userId, UUID roleId) {
    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(() -> new OrganizationNotFoundException(organizationId));

    User user =
        userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

    Role role =
        roleRepository.findById(roleId).orElseThrow(() -> new RoleNotFoundException(roleId));

    OrganizationUserRoleKey key = new OrganizationUserRoleKey(organizationId, userId, roleId);
    OrganizationUserRole our = new OrganizationUserRole(key);
    our.setOrganization(organization);
    our.setUser(user);
    our.setRole(role);
    ourRepository.save(our);

    organization.getRoles().add(our);
    user.getRoles().add(our);
    role.getRoles().add(our);

    organizationRepository.save(organization);
    userRepository.save(user);
    roleRepository.save(role);

    return convertToDTO(our);
  }

  public void deleteUserRoleFromOrganization(UUID organizationId, UUID userId, UUID roleId) {
    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(() -> new OrganizationNotFoundException(organizationId));

    User user =
        userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

    Role role =
        roleRepository.findById(roleId).orElseThrow(() -> new RoleNotFoundException(roleId));

    OrganizationUserRole our =
        organization.getRoles().stream()
            .filter(
                our_tmp ->
                    our_tmp.getId().getUserId().equals(userId)
                        && our_tmp.getId().getRoleId().equals(roleId))
            .findFirst()
            .orElseThrow(
                () -> new WrongRoleForUserInOrganizationException(userId, roleId, organizationId));

    organization.getRoles().remove(our);
    user.getRoles().remove(our);
    role.getRoles().remove(our);

    organizationRepository.save(organization);
    userRepository.save(user);
    roleRepository.save(role);

    ourRepository.delete(our);
  }

  public static UserRoleDto convertToDTO(OrganizationUserRole our) {
    UserDTO userDTO = UserService.convertToDTO(our.getUser());
    RoleDTO roleDTO = RoleService.convertToDTO(our.getRole());

    return new UserRoleDto(userDTO, roleDTO);
  }
}
