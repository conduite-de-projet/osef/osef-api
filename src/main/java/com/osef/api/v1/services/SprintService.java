package com.osef.api.v1.services;

import com.osef.api.v1.enums.SprintBlockers;
import com.osef.api.v1.enums.SprintStatus;
import com.osef.api.v1.enums.TicketStatus;
import com.osef.api.v1.exceptions.ProjectNotFoundException;
import com.osef.api.v1.exceptions.SprintNotFoundException;
import com.osef.api.v1.exceptions.TicketNotFoundException;
import com.osef.api.v1.exceptions.UserNotFoundException;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.Sprint;
import com.osef.api.v1.models.Ticket;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.SprintDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.repositories.ProjectRepository;
import com.osef.api.v1.repositories.SprintRepository;
import com.osef.api.v1.repositories.TicketRepository;
import com.osef.api.v1.repositories.UserRepository;
import com.osef.api.v1.repositories.specifications.SprintSpecification;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class SprintService {

  @Autowired private SprintRepository sprintRepository;
  @Autowired private ProjectRepository projectRepository;
  @Autowired private UserService userService;
  @Autowired private TicketService ticketService;
  @Autowired private TicketRepository ticketRepository;
  @Autowired private UserRepository userRepository;

  public SprintDTO createSprint(SprintDTO sprintDTO) {
    Project project =
        projectRepository
            .findById(sprintDTO.projectId())
            .orElseThrow(() -> new ProjectNotFoundException(sprintDTO.projectId()));

    Sprint previousSprint = null;
    Sprint nextSprint = null;
    Sprint dependentSprint = null;

    if (sprintDTO.previousSprintId() != null) {
      previousSprint =
          sprintRepository
              .findById(sprintDTO.previousSprintId())
              .orElseThrow(
                  () ->
                      new IllegalArgumentException(
                          "Previous sprint not found: " + sprintDTO.previousSprintId()));
    }

    if (sprintDTO.nextSprintId() != null) {
      nextSprint =
          sprintRepository
              .findById(sprintDTO.nextSprintId())
              .orElseThrow(
                  () ->
                      new IllegalArgumentException(
                          "Next sprint not found: " + sprintDTO.nextSprintId()));
    }

    if (sprintDTO.dependentSprintId() != null) {
      dependentSprint =
          sprintRepository
              .findById(sprintDTO.dependentSprintId())
              .orElseThrow(
                  () ->
                      new IllegalArgumentException(
                          "Dependent sprint not found: " + sprintDTO.dependentSprintId()));
    }

    Sprint sprint =
        new Sprint(
            project,
            sprintDTO.name(),
            sprintDTO.goals(),
            sprintDTO.status(),
            sprintDTO.startDate(),
            sprintDTO.endDate(),
            sprintDTO.velocity(),
            previousSprint,
            nextSprint,
            dependentSprint);

    if (sprintDTO.blockers() != null) {
      sprint.getBlockers().addAll(Set.of(sprintDTO.blockers()));
    }

    return convertToDTO(sprintRepository.save(sprint));
  }

  public SprintDTO createSprint(SprintDTO sprintDTO, Project project) {
    Sprint previousSprint = null;
    Sprint nextSprint = null;
    Sprint dependentSprint = null;

    // Récupération des relations si elles existent
    if (sprintDTO.previousSprintId() != null) {
      previousSprint =
          sprintRepository
              .findById(sprintDTO.previousSprintId())
              .orElseThrow(() -> new IllegalArgumentException("Previous sprint not found"));
    }

    if (sprintDTO.nextSprintId() != null) {
      nextSprint =
          sprintRepository
              .findById(sprintDTO.nextSprintId())
              .orElseThrow(() -> new IllegalArgumentException("Next sprint not found"));
    }

    if (sprintDTO.dependentSprintId() != null) {
      dependentSprint =
          sprintRepository
              .findById(sprintDTO.dependentSprintId())
              .orElseThrow(() -> new IllegalArgumentException("Dependent sprint not found"));
    }

    // Création du sprint
    Sprint sprint =
        new Sprint(
            project,
            sprintDTO.name(),
            sprintDTO.goals(),
            sprintDTO.status(),
            sprintDTO.startDate(),
            sprintDTO.endDate(),
            sprintDTO.velocity(),
            previousSprint,
            nextSprint,
            dependentSprint);

    if (sprintDTO.blockers() != null) {
      sprint.getBlockers().addAll(Set.of(sprintDTO.blockers()));
    }

    project.getSprints().add(sprint);
    projectRepository.save(project);

    return convertToDTO(sprint);
  }

  public void deleteSprint(UUID id) {
    sprintRepository.deleteById(id);
  }

  public SprintDTO updateSprint(UUID id, SprintDTO updatedSprint) {
    return convertToDTO(
        sprintRepository
            .findById(id)
            .map(
                sprint -> {
                  SprintStatus originalStatus = sprint.getStatus();
                  if (originalStatus.equals(SprintStatus.PLANNED)
                      && updatedSprint.status().equals(SprintStatus.IN_PROGRESS)) {
                    sprint.setActualStartDate(Timestamp.from(Instant.now()));
                  }
                  if (originalStatus.equals(SprintStatus.IN_PROGRESS)
                      && updatedSprint.status().equals(SprintStatus.COMPLETED)) {
                    sprint.setActualEndDate(Timestamp.from(Instant.now()));
                  }

                  sprint.setName(updatedSprint.name());
                  sprint.setStatus(updatedSprint.status());
                  sprint.setStartDate(updatedSprint.startDate());
                  sprint.setEndDate(updatedSprint.endDate());
                  sprint.setVelocity(updatedSprint.velocity());
                  sprint.setUpdatedAt(Timestamp.from(Instant.now()));
                  return sprintRepository.save(sprint);
                })
            .orElseThrow(() -> new TicketNotFoundException(id)));
  }

  public SprintDTO getSprintById(UUID id) {
    return sprintRepository
        .findById(id)
        .map(SprintService::convertToDTO)
        .orElseThrow(() -> new TicketNotFoundException(id));
  }

  public Sprint getSprintByIdRaw(UUID id) {
    return sprintRepository.findById(id).orElseThrow(() -> new TicketNotFoundException(id));
  }

  public List<SprintDTO> getAllSprints() {
    return filterSprint(null, null).stream().map(SprintService::convertToDTO).toList();
  }

  public List<SprintDTO> getAllSprints(SprintStatus status) {
    return filterSprint(null, status).stream().map(SprintService::convertToDTO).toList();
  }

  public List<SprintDTO> getAllSprints(UUID projectId, SprintStatus status) {
    return filterSprint(projectId, status).stream().map(SprintService::convertToDTO).toList();
  }

  public List<Sprint> getAllRaw(UUID projectId, SprintStatus status) {
    return filterSprint(projectId, status);
  }

  public static SprintDTO convertToDTO(Sprint sprint) {
    return new SprintDTO(
        sprint.getId(),
        (sprint.getProject() != null) ? sprint.getProject().getId() : null,
        (sprint.getPreviousSprint() != null) ? sprint.getPreviousSprint().getId() : null,
        (sprint.getNextSprint() != null) ? sprint.getNextSprint().getId() : null,
        (sprint.getDependentSprint() != null) ? sprint.getDependentSprint().getId() : null,
        sprint.getReference(),
        sprint.getName(),
        sprint.getGoals(),
        sprint.getStatus(),
        sprint.getStartDate(),
        sprint.getActualStartDate(),
        sprint.getEndDate(),
        sprint.getActualEndDate(),
        sprint.getVelocity(),
        sprint.getBlockers().toArray(new SprintBlockers[0]),
        sprint.getCarriedTickets(),
        sprint.getCreatedAt(),
        sprint.getUpdatedAt());
  }

  public List<Sprint> filterSprint(UUID projectId, SprintStatus status) {
    Specification<Sprint> spec = Specification.where(null);
    if (projectId != null) {
      spec = spec.and(SprintSpecification.hasProjectId(projectId));
    }
    if (status != null) {
      spec = spec.and(SprintSpecification.hasStatus(status));
    }

    return sprintRepository.findAll(spec);
  }

  public List<TicketDTO> getSprintTickets(UUID sprintId) {
    return getSprintByIdRaw(sprintId).getTickets().stream()
        .map(TicketService::convertToDTO)
        .toList();
  }

  public SprintDTO addTicket(UUID sprintId, UUID ticketId) {
    Sprint sprint = getSprintByIdRaw(sprintId);
    Ticket ticket = ticketService.getTicketByIdRaw(ticketId);

    if (ticket.getSprint() != null && !ticket.getSprint().getId().equals(sprintId)) {
      throw new IllegalStateException(
          String.format("Ticket %s is already associated with another sprint", ticketId));
    }

    ticket.setSprint(sprint);
    sprint.getTickets().add(ticket);

    sprintRepository.save(sprint);
    ticketRepository.save(ticket);

    return convertToDTO(sprint);
  }

  public SprintDTO createTicket(UUID sprintId, TicketDTO ticketDTO) {
    Sprint sprint = getSprintByIdRaw(sprintId);
    Project project = sprint.getProject();

    if (project == null) {
      throw new IllegalArgumentException("Project must not be null");
    }

    if (ticketDTO.authorId() == null) {
      throw new IllegalArgumentException("Author must not be null");
    }

    User assignee = null;
    if (ticketDTO.assigneeId() != null) {
      assignee =
          userRepository
              .findById(ticketDTO.assigneeId())
              .orElseThrow(() -> new UserNotFoundException(ticketDTO.assigneeId()));
    }

    User author =
        userRepository
            .findById(ticketDTO.authorId())
            .orElseThrow(() -> new UserNotFoundException(ticketDTO.authorId()));

    Ticket ticket =
        new Ticket(
            ticketDTO.parentTicketId(),
            ticketDTO.type(),
            ticketDTO.status(),
            ticketDTO.title(),
            ticketDTO.description(),
            ticketDTO.priority(),
            ticketDTO.estimatedTime(),
            ticketDTO.timeSpent(),
            ticketDTO.deadline());

    ticket.setProject(project);
    ticket.setAuthor(author);

    if (assignee != null) {
      ticket.setAssignee(assignee);
    }

    ticket.setReference(
        TicketService.TicketCodeGenerator.calculateNextReference(
            project.getTickets().stream().toList()));
    ticket.setCode(
        TicketService.TicketCodeGenerator.generateTicketCode(project, ticket.getReference()));

    sprint.getTickets().add(ticket);
    ticket.setSprint(sprint);
    return convertToDTO(sprintRepository.save(sprint));
  }

  public void removeTicket(UUID sprintId, UUID ticketId) {
    Sprint sprint = getSprintByIdRaw(sprintId);
    Ticket ticket = ticketService.getTicketByIdRaw(ticketId);

    if (ticket.getSprint() == null || !ticket.getSprint().getId().equals(sprintId)) {
      throw new IllegalStateException(
          String.format("Ticket %s is not associated with the sprint %s", ticketId, sprintId));
    }

    ticket.setSprint(null);
    sprint.getTickets().remove(ticket);

    sprintRepository.save(sprint);
    ticketRepository.save(ticket);
  }

  public Map<String, List<TicketDTO>> getTicketsDoneLastTwoWeeks(UUID id) {
    Sprint sprint =
        sprintRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    Instant now = Instant.now().plus(1, ChronoUnit.DAYS);

    Instant startOfLastWeek = now.minus(7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
    Instant startOfTwoWeeksAgo = now.minus(14, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
    Instant endOfLastWeek = startOfLastWeek.plus(7, ChronoUnit.DAYS);

    List<TicketDTO> ticketsLastWeek =
        sprint.getTickets().stream()
            .filter(
                ticket ->
                    ticket.getDoneAt() != null
                        && ticket.getDoneAt().toInstant().isAfter(startOfLastWeek)
                        && ticket.getDoneAt().toInstant().isBefore(endOfLastWeek))
            .map(TicketService::convertToDTO)
            .toList();

    List<TicketDTO> ticketsTwoWeeksAgo =
        sprint.getTickets().stream()
            .filter(
                ticket ->
                    ticket.getDoneAt() != null
                        && ticket.getDoneAt().toInstant().isAfter(startOfTwoWeeksAgo)
                        && ticket.getDoneAt().toInstant().isBefore(startOfLastWeek))
            .map(TicketService::convertToDTO)
            .toList();

    Map<String, List<TicketDTO>> result = new HashMap<>();
    result.put("lastWeek", ticketsLastWeek);
    result.put("twoWeeksAgo", ticketsTwoWeeksAgo);

    return result;
  }

  public SprintDTO terminateSprint(UUID sprintId) {
    Sprint sprint =
        sprintRepository
            .findById(sprintId)
            .orElseThrow(() -> new SprintNotFoundException(sprintId));

    List<Ticket> ticketsToTransfer =
        sprint.getTickets().stream()
            .filter(ticket -> ticket.getStatus() != TicketStatus.DONE)
            .toList();

    int velocity =
        sprint.getTickets().stream()
            .filter(ticket -> ticket.getStatus() == TicketStatus.DONE)
            .mapToInt(
                ticket ->
                    (ticket.getEstimatedTime() != null && ticket.getEstimatedTime() > 0)
                        ? ticket.getEstimatedTime()
                        : 1)
            .sum();

    sprint.setVelocity(velocity);

    Sprint nextPlannedSprint =
        sprintRepository.findFirstByProjectAndReferenceGreaterThanAndStatus(
            sprint.getProject(), sprint.getReference(), SprintStatus.PLANNED);

    if (nextPlannedSprint == null) {
      for (Ticket ticket : ticketsToTransfer) {
        ticket.setSprint(null);
        ticketRepository.save(ticket);
      }
    } else {
      for (Ticket ticket : ticketsToTransfer) {
        ticket.setSprint(nextPlannedSprint);
        ticketRepository.save(ticket);
      }

      nextPlannedSprint.setCarriedTickets(ticketsToTransfer.size());

      nextPlannedSprint.setPreviousSprint(sprint);
      sprint.setNextSprint(nextPlannedSprint);

      sprintRepository.save(nextPlannedSprint);
    }

    sprint.setStatus(SprintStatus.COMPLETED);
    sprint.setActualEndDate(Timestamp.from(Instant.now()));

    return convertToDTO(sprintRepository.save(sprint));
  }

  public SprintDTO startSprint(UUID sprintId) {
    Sprint sprint =
        sprintRepository
            .findById(sprintId)
            .orElseThrow(() -> new SprintNotFoundException(sprintId));

    if (sprint.getStatus() != SprintStatus.PLANNED) {
      throw new IllegalStateException(
          String.format("Cannot start sprint %s as it is not in PLANNED state.", sprintId));
    }

    if (sprint.getPreviousSprint() != null
        && sprint.getPreviousSprint().getStatus() != SprintStatus.COMPLETED) {
      throw new IllegalStateException(
          String.format(
              "Cannot start sprint %s as the previous sprint is not COMPLETED.", sprintId));
    }

    if (sprint.getTickets().isEmpty()) {
      throw new IllegalStateException(
          String.format("Cannot start sprint %s as it does not contain any tickets.", sprintId));
    }

    sprint.setStatus(SprintStatus.IN_PROGRESS);
    sprint.setActualStartDate(Timestamp.from(Instant.now()));

    return convertToDTO(sprintRepository.save(sprint));
  }
}
