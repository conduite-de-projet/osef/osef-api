package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.ProjectNotFoundException;
import com.osef.api.v1.exceptions.RepositoryNotFoundException;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.Repository;
import com.osef.api.v1.models.dto.RepositoryDTO;
import com.osef.api.v1.repositories.ProjectRepository;
import com.osef.api.v1.repositories.RepositoryRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryService {

  @Autowired private RepositoryRepository repositoryRepository;
  @Autowired private ProjectRepository projectRepository;

  public RepositoryDTO createRepository(RepositoryDTO repositoryDTO, Project project) {
    Repository repository =
        new Repository(
            project,
            repositoryDTO.remoteId(),
            repositoryDTO.type(),
            repositoryDTO.host(),
            repositoryDTO.name(),
            repositoryDTO.token(),
            repositoryDTO.tokenExpiringDate(),
            repositoryDTO.lastSynced(),
            Timestamp.from(Instant.now()),
            Timestamp.from(Instant.now()));

    project.getRepositories().add(repository);
    projectRepository.save(project);

    return convertToDTO(repository);
  }

  public List<RepositoryDTO> getAllRepositories(UUID projectId) {
    return repositoryRepository.findAll().stream().map(RepositoryService::convertToDTO).toList();
  }

  public List<Repository> getAllRaw(UUID projectId) {
    return repositoryRepository.findAll();
  }

  public RepositoryDTO getRepositoryById(UUID id) {
    return repositoryRepository
        .findById(id)
        .map(RepositoryService::convertToDTO)
        .orElseThrow(() -> new RepositoryNotFoundException(id));
  }

  public RepositoryDTO updateRepository(UUID id, RepositoryDTO updatedRepository) {
    return repositoryRepository
        .findById(id)
        .map(
            repository -> {
              repository.setName(updatedRepository.name());
              repository.setRemoteId(updatedRepository.remoteId());
              repository.setType(updatedRepository.type());
              repository.setHost(updatedRepository.host());
              repository.setToken(updatedRepository.token());
              repository.setTokenExpiringDate(updatedRepository.tokenExpiringDate());
              repository.setLastSynced(updatedRepository.lastSynced());

              return convertToDTO(repositoryRepository.save(repository));
            })
        .orElseThrow(() -> new ProjectNotFoundException(id));
  }

  public void deleteProject(UUID id) {
    repositoryRepository.deleteById(id);
  }

  public static RepositoryDTO convertToDTO(Repository repository) {
    return new RepositoryDTO(
        repository.getId(),
        repository.getProject().getId(),
        repository.getRemoteId(),
        repository.getType(),
        repository.getHost(),
        repository.getName(),
        repository.getToken(),
        repository.getTokenExpiringDate(),
        repository.getLastSynced(),
        repository.getCreatedAt(),
        repository.getUpdatedAt());
  }
}
