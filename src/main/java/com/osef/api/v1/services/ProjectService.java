package com.osef.api.v1.services;

import com.osef.api.v1.enums.SprintStatus;
import com.osef.api.v1.exceptions.OrganizationNotFoundException;
import com.osef.api.v1.exceptions.ProjectNotFoundException;
import com.osef.api.v1.models.Organization;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.dto.ProjectDTO;
import com.osef.api.v1.models.dto.ProjectRepositoryDTO;
import com.osef.api.v1.models.dto.RepositoryDTO;
import com.osef.api.v1.models.dto.SprintDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.repositories.OrganizationRepository;
import com.osef.api.v1.repositories.ProjectRepository;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectService {

  @Autowired private ProjectRepository projectRepository;
  @Autowired private RepositoryService repositoryService;
  @Autowired private OrganizationRepository organizationRepository;
  @Autowired private TicketService ticketService;
  @Autowired private SprintService sprintService;

  public ProjectDTO createProject(ProjectDTO projectDTO) {
    Project project =
        new Project(
            projectDTO.name(),
            projectDTO.status(),
            projectDTO.startDate(),
            projectDTO.visibility(),
            Timestamp.from(java.time.Instant.now()),
            Timestamp.from(java.time.Instant.now()),
            projectDTO.description(),
            projectDTO.endDate(),
            null,
            null,
            null,
            projectDTO.icon());
    return convertToDTO(projectRepository.save(project));
  }

  public ProjectDTO createProject(ProjectDTO projectDTO, UUID organizationId) {
    Project project =
        new Project(
            projectDTO.name(),
            projectDTO.status(),
            projectDTO.startDate(),
            projectDTO.visibility(),
            Timestamp.from(java.time.Instant.now()),
            Timestamp.from(java.time.Instant.now()),
            projectDTO.description(),
            projectDTO.endDate(),
            null,
            null,
            null,
            projectDTO.icon());

    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(() -> new OrganizationNotFoundException(organizationId));
    project.setOrganization(organization);
    return convertToDTO(projectRepository.save(project));
  }

  public List<ProjectDTO> getAllProjects() {
    return projectRepository.findAll().stream().map(ProjectService::convertToDTO).toList();
  }

  public ProjectDTO getProjectById(UUID id) {
    return projectRepository
        .findById(id)
        .map(ProjectService::convertToDTO)
        .orElseThrow(() -> new ProjectNotFoundException(id));
  }

  public ProjectDTO updateProject(UUID id, ProjectDTO updatedProject) {
    return projectRepository
        .findById(id)
        .map(
            project -> {
              project.setName(updatedProject.name());
              project.setDescription(updatedProject.description());
              project.setStatus(updatedProject.status());
              project.setVisibility(updatedProject.visibility());
              project.setEndDate(updatedProject.endDate());
              project.setUpdatedAt(Timestamp.from(java.time.Instant.now()));
              project.setIcon(updatedProject.icon());
              return convertToDTO(projectRepository.save(project));
            })
        .orElseThrow(() -> new ProjectNotFoundException(id));
  }

  public void deleteProject(UUID id) {
    projectRepository.deleteById(id);
  }

  public static ProjectDTO convertToDTO(Project project) {
    return new ProjectDTO(
        project.getId(),
        project.getName(),
        project.getDescription(),
        project.getStatus(),
        project.getStartDate(),
        project.getEndDate(),
        project.getVisibility(),
        project.getCreatedAt(),
        project.getUpdatedAt(),
        project.getIcon());
  }

  public ProjectRepositoryDTO createRepository(UUID id, RepositoryDTO repositoryDTO) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    RepositoryDTO repository = repositoryService.createRepository(repositoryDTO, project);

    Set<RepositoryDTO> repositories =
        project.getRepositories().stream()
            .map(RepositoryService::convertToDTO)
            .collect(Collectors.toSet());

    return new ProjectRepositoryDTO(
        project.getId(),
        project.getName(),
        project.getDescription(),
        project.getStatus(),
        project.getStartDate(),
        project.getEndDate(),
        project.getVisibility(),
        project.getCreatedAt(),
        project.getUpdatedAt(),
        project.getIcon(),
        repositories);
  }

  public List<RepositoryDTO> getRepositories(UUID id) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return repositoryService.getAllRaw(project.getId()).stream()
        .filter(repository -> repository.getProject().equals(project))
        .map(RepositoryService::convertToDTO)
        .toList();
  }

  public TicketDTO createTicket(UUID id, TicketDTO ticketDTO) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return ticketService.createTicket(ticketDTO, project);
  }

  public TicketDTO createRepositoryTicket(
      UUID id, TicketDTO ticketDTO, UUID repositoryId, Integer remoteId) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return ticketService.createTicket(ticketDTO, project, repositoryId, remoteId);
  }

  public List<TicketDTO> getTickets(UUID id) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return project.getTickets().stream().map(TicketService::convertToDTO).toList();
  }

  public Map<String, List<TicketDTO>> getTicketsDoneLastTwoWeeks(UUID id) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    Instant now = Instant.now().plus(1, ChronoUnit.DAYS);

    Instant startOfLastWeek = now.minus(7, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
    Instant startOfTwoWeeksAgo = now.minus(14, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
    Instant endOfLastWeek = startOfLastWeek.plus(7, ChronoUnit.DAYS);

    List<TicketDTO> ticketsLastWeek =
        project.getTickets().stream()
            .filter(
                ticket ->
                    ticket.getDoneAt() != null
                        && ticket.getDoneAt().toInstant().isAfter(startOfLastWeek)
                        && ticket.getDoneAt().toInstant().isBefore(endOfLastWeek))
            .map(TicketService::convertToDTO)
            .toList();

    List<TicketDTO> ticketsTwoWeeksAgo =
        project.getTickets().stream()
            .filter(
                ticket ->
                    ticket.getDoneAt() != null
                        && ticket.getDoneAt().toInstant().isAfter(startOfTwoWeeksAgo)
                        && ticket.getDoneAt().toInstant().isBefore(startOfLastWeek))
            .map(TicketService::convertToDTO)
            .toList();

    Map<String, List<TicketDTO>> result = new HashMap<>();
    result.put("lastWeek", ticketsLastWeek);
    result.put("twoWeeksAgo", ticketsTwoWeeksAgo);

    return result;
  }

  public SprintDTO createSprint(UUID id, SprintDTO sprintDTO) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return sprintService.createSprint(sprintDTO, project);
  }

  public List<SprintDTO> getSprints(UUID id, SprintStatus status) {
    Project project =
        projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));

    return project.getSprints().stream().map(SprintService::convertToDTO).toList();
  }
}
