package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.LabelNotFoundException;
import com.osef.api.v1.models.Label;
import com.osef.api.v1.models.dto.LabelDTO;
import com.osef.api.v1.repositories.LabelRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LabelService {

  @Autowired private LabelRepository labelRepository;

  public LabelDTO createLabel(LabelDTO labelDTO) {
    Label label = new Label(labelDTO.name(), labelDTO.color());
    return convertToDTO(labelRepository.save(label));
  }

  public List<LabelDTO> getLabels() {
    return labelRepository.findAll().stream().map(LabelService::convertToDTO).toList();
  }

  public LabelDTO getLabelById(UUID id) {
    Label label = labelRepository.findById(id).orElseThrow(() -> new LabelNotFoundException(id));
    return convertToDTO(label);
  }

  public static LabelDTO convertToDTO(Label label) {
    return new LabelDTO(label.getId(), label.getName(), label.getColor());
  }
}
