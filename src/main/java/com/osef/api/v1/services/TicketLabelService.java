package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.LabelNotFoundException;
import com.osef.api.v1.exceptions.TicketNotFoundException;
import com.osef.api.v1.models.Label;
import com.osef.api.v1.models.Ticket;
import com.osef.api.v1.models.TicketLabel;
import com.osef.api.v1.models.TicketLabelKey;
import com.osef.api.v1.models.dto.LabelDTO;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.repositories.LabelRepository;
import com.osef.api.v1.repositories.TicketLabelRepository;
import com.osef.api.v1.repositories.TicketRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketLabelService {

  @Autowired private TicketLabelRepository ticketLabelRepository;

  @Autowired private TicketRepository ticketRepository;

  @Autowired private LabelRepository labelRepository;

  public TicketDTO addLabelToTocket(UUID ticketId, UUID labelId) {
    Ticket ticket =
        ticketRepository
            .findById(ticketId)
            .orElseThrow(() -> new TicketNotFoundException(ticketId));
    Label label =
        labelRepository.findById(labelId).orElseThrow(() -> new LabelNotFoundException(labelId));

    TicketLabelKey key = new TicketLabelKey(ticketId, labelId);
    TicketLabel ticketLabel = new TicketLabel(key);

    ticketLabel.setTicket(ticket);
    ticketLabel.setLabel(label);

    ticket.getTicketLabels().add(ticketLabel);
    label.getTickets().add(ticketLabel);

    ticketLabelRepository.save(ticketLabel);
    labelRepository.save(label);
    return TicketService.convertToDTO(ticketRepository.save(ticket));
  }

  public List<LabelDTO> getLabelsFromTicket(UUID ticketId) {
    Ticket ticket =
        ticketRepository
            .findById(ticketId)
            .orElseThrow(() -> new TicketNotFoundException(ticketId));
    return ticket.getLabels().stream().map(LabelService::convertToDTO).toList();
  }

  public void removeLabelFromTicket(UUID ticketId, UUID labelId) {
    Ticket ticket =
        ticketRepository
            .findById(ticketId)
            .orElseThrow(() -> new TicketNotFoundException(ticketId));

    Label label =
        labelRepository.findById(labelId).orElseThrow(() -> new LabelNotFoundException(labelId));

    TicketLabelKey key = new TicketLabelKey(ticketId, labelId);
    TicketLabel ticketLabel =
        ticketLabelRepository
            .findById(key)
            .orElseThrow(() -> new TicketNotFoundException(ticketId));

    ticket.getLabels().remove(ticketLabel);
    label.getTickets().remove(ticketLabel);

    ticketRepository.save(ticket);
    labelRepository.save(label);
    ticketLabelRepository.delete(ticketLabel);
  }
}
