package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.UserNotFoundException;
import com.osef.api.v1.exceptions.UserNotInOrganization;
import com.osef.api.v1.exceptions.WrongPasswordException;
import com.osef.api.v1.models.OrganizationUserRole;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.LoginDTO;
import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.repositories.UserRepository;
import com.osef.api.v1.repositories.specifications.UserSpecification;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  @Autowired private UserRepository userRepository;

  public UserDTO createUser(UserDTO userDTO) {
    byte[] salt = generateSalt();
    byte[] hash = generateHash(userDTO.passwordHash(), salt);

    User user =
        new User(
            userDTO.firstName(),
            userDTO.lastName(),
            userDTO.email(),
            hash,
            salt,
            userDTO.profileColor(),
            userDTO.city(),
            userDTO.zipCode(),
            userDTO.address(),
            userDTO.state(),
            userDTO.bio(),
            userDTO.website(),
            userDTO.status());
    return convertToDTO(userRepository.save(user));
  }

  public void deleteUser(UUID id) {
    userRepository.deleteById(id);
  }

  public UserDTO getUserById(UUID id) {
    return userRepository
        .findById(id)
        .map(UserService::convertToDTO)
        .orElseThrow(() -> new UserNotFoundException(id));
  }

  public OrganizationDTO getUserOrganization(UUID id) {
    return userRepository
        .findById(id)
        .map(
            (user) -> {
              if (user.getRoles().isEmpty()) {
                throw new UserNotInOrganization(id);
              }

              Set<OrganizationUserRole> roles = user.getRoles();
              Object[] rolesArray = roles.toArray();
              return OrganizationService.convertToDTO(
                  ((OrganizationUserRole) rolesArray[0]).getOrganization());
            })
        .orElseThrow(() -> new UserNotFoundException(id));
  }

  public List<UserDTO> getAllUsers() {
    return userRepository.findAll().stream().map(UserService::convertToDTO).toList();
  }

  public UserDTO updateUser(UUID id, UserDTO updatedUser) {
    return convertToDTO(
        userRepository
            .findById(id)
            .map(
                user -> {
                  user.setFirstName(updatedUser.firstName());
                  user.setLastName(updatedUser.lastName());
                  user.setEmail(updatedUser.email());
                  user.setProfileColor(updatedUser.profileColor());
                  user.setCity(updatedUser.city());
                  user.setZipCode(updatedUser.zipCode());
                  user.setAddress(updatedUser.address());
                  user.setState(updatedUser.state());
                  user.setBio(updatedUser.bio());
                  user.setWebsite(updatedUser.website());
                  user.setStatus(updatedUser.status());

                  user.setLastLogin(updatedUser.lastLogin());
                  user.setUpdatedAt(Timestamp.from(java.time.Instant.now()));
                  return userRepository.save(user);
                })
            .orElseThrow(() -> new UserNotFoundException(id)));
  }

  public List<UserDTO> filterUsers(String name, String email) {
    Specification<User> spec = Specification.where(null);

    if (name != null && !name.isEmpty()) {
      spec = spec.and(UserSpecification.hasName(name));
    }

    if (email != null && !email.isEmpty()) {
      spec = spec.and(UserSpecification.hasEmail(email));
    }

    return userRepository.findAll(spec).stream().map(UserService::convertToDTO).toList();
  }

  public UserDTO login(LoginDTO loginDTO) {
    Specification<User> spec = Specification.where(UserSpecification.hasEmail(loginDTO.email()));
    return userRepository
        .findOne(spec)
        .map(
            foundUser -> {
              byte[] hash = generateHash(loginDTO.passwordHash(), foundUser.getSalt());
              if (Arrays.equals(hash, foundUser.getPasswordHash())) {
                foundUser.setLastLogin(Timestamp.from(java.time.Instant.now()));
                return convertToDTO(userRepository.save(foundUser));
              }
              throw new WrongPasswordException();
            })
        .orElseThrow(() -> new UserNotFoundException(loginDTO.email()));
  }

  public static UserDTO convertToDTO(User user) {
    return new UserDTO(
        user.getId(),
        user.getFirstName(),
        user.getLastName(),
        user.getEmail(),
        "",
        user.getProfileColor(),
        user.getCity(),
        user.getZipCode(),
        user.getAddress(),
        user.getState(),
        user.getBio(),
        user.getWebsite(),
        user.getStatus(),
        user.getCreatedAt(),
        user.getUpdatedAt(),
        user.getLastLogin());
  }

  private byte[] generateSalt() {
    SecureRandom random = new SecureRandom();
    byte[] salt = new byte[16];
    random.nextBytes(salt);
    return salt;
  }

  private byte[] generateHash(String password, byte[] salt) {
    try {
      KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      return factory.generateSecret(spec).getEncoded();
    } catch (Exception e) {
      throw new RuntimeException("Le hashage a planté... C'est ballot");
    }
  }
}
