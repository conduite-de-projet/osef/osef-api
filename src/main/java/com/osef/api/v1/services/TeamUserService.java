package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.TeamNotFoundException;
import com.osef.api.v1.exceptions.UserNotFoundException;
import com.osef.api.v1.exceptions.UserNotInTeamException;
import com.osef.api.v1.models.Team;
import com.osef.api.v1.models.TeamUser;
import com.osef.api.v1.models.TeamUserKey;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.repositories.TeamRepository;
import com.osef.api.v1.repositories.TeamUserRepository;
import com.osef.api.v1.repositories.UserRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamUserService {

  @Autowired private TeamUserRepository teamUserRepository;

  @Autowired private UserRepository userRepository;

  @Autowired private TeamRepository teamRepository;

  public TeamDTO addUserToTeam(UUID teamId, UUID userId) {
    Team team =
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));
    User user =
        userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

    TeamUserKey key = new TeamUserKey(teamId, userId);
    TeamUser teamUser = new TeamUser(key);
    teamUser.setTeam(team);
    teamUser.setUser(user);

    team.getUsers().add(teamUser);
    user.getTeams().add(teamUser);

    teamUserRepository.save(teamUser);
    userRepository.save(user);
    return TeamService.convertToDTO(teamRepository.save(team));
  }

  public List<UserDTO> getUsersFromTeam(UUID teamId) {
    Team team =
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));
    return team.getUsers().stream()
        .map(teamUser -> teamUser.getUser())
        .map(UserService::convertToDTO)
        .toList();
  }

  public void removeUserFromTeam(UUID teamId, UUID userId) {
    Team team =
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));

    User user =
        userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

    TeamUser teamUser =
        team.getUsers().stream()
            .filter(tu -> tu.getUser().getId().equals(userId))
            .findFirst()
            .orElseThrow(() -> new UserNotInTeamException(userId, teamId));

    team.getUsers().remove(teamUser);
    user.getTeams().remove(teamUser);

    teamUserRepository.delete(teamUser);
    userRepository.save(user);
    teamRepository.save(team);
  }
}
