package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.OrganizationNotFoundException;
import com.osef.api.v1.exceptions.TeamNotFoundException;
import com.osef.api.v1.exceptions.TeamNotInOrganizationException;
import com.osef.api.v1.models.Organization;
import com.osef.api.v1.models.Team;
import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.repositories.OrganizationRepository;
import com.osef.api.v1.repositories.TeamRepository;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService {

  @Autowired private OrganizationRepository organizationRepository;

  @Autowired private TeamRepository teamRepository;

  public List<OrganizationDTO> getAllOrganizations() {
    return organizationRepository.findAll().stream()
        .map(OrganizationService::convertToDTO)
        .toList();
  }

  public OrganizationDTO createOrganization(OrganizationDTO organizationDTO) {
    Organization organization =
        new Organization(organizationDTO.name(), organizationDTO.description());
    return convertToDTO(organizationRepository.save(organization));
  }

  public OrganizationDTO getOrganizationById(UUID id) {
    return organizationRepository
        .findById(id)
        .map(OrganizationService::convertToDTO)
        .orElseThrow(() -> new OrganizationNotFoundException(id));
  }

  public OrganizationDTO updateOrganization(UUID id, OrganizationDTO updatedOrganization) {
    return convertToDTO(
        organizationRepository
            .findById(id)
            .map(
                organization -> {
                  organization.setName(updatedOrganization.name());
                  organization.setDescription(updatedOrganization.description());
                  organization.setUpdatedAt(Timestamp.from(java.time.Instant.now()));
                  return organizationRepository.save(organization);
                })
            .orElseThrow(() -> new OrganizationNotFoundException(id)));
  }

  public void deleteOrganization(UUID id) {
    organizationRepository.deleteById(id);
  }

  public List<TeamDTO> getTeamsFromOrganization(UUID organizationId) {
    return organizationRepository
        .findById(organizationId)
        .map(
            organization ->
                organization.getTeams().stream().map(TeamService::convertToDTO).toList())
        .orElseThrow(() -> new OrganizationNotFoundException(organizationId));
  }

  public OrganizationDTO addTeamToOrganization(UUID organizationId, UUID teamId) {
    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(
                () -> new RuntimeException("Organization not found with id: " + organizationId));

    Team team =
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));

    team.setOrganization(organization);
    organization.getTeams().add(team);

    teamRepository.save(team);
    return convertToDTO(organizationRepository.save(organization));
  }

  public void removeTeamFromOrganization(UUID organizationId, UUID teamId) {
    Organization organization =
        organizationRepository
            .findById(organizationId)
            .orElseThrow(() -> new OrganizationNotFoundException(organizationId));

    Team team =
        teamRepository.findById(teamId).orElseThrow(() -> new TeamNotFoundException(teamId));

    if (!organization.getTeams().remove(team)) {
      throw new TeamNotInOrganizationException(teamId, organizationId);
    }
    team.setOrganization(null);

    teamRepository.save(team);
    organizationRepository.save(organization);
  }

  public static OrganizationDTO convertToDTO(Organization organization) {
    return new OrganizationDTO(
        organization.getId(),
        organization.getName(),
        organization.getDescription(),
        organization.getCreatedAt(),
        organization.getUpdatedAt());
  }
}
