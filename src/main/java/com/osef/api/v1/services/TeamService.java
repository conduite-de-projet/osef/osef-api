package com.osef.api.v1.services;

import com.osef.api.v1.exceptions.NoOrganizationForTeamException;
import com.osef.api.v1.exceptions.TeamNotFoundException;
import com.osef.api.v1.models.Team;
import com.osef.api.v1.models.dto.OrganizationDTO;
import com.osef.api.v1.models.dto.TeamDTO;
import com.osef.api.v1.repositories.TeamRepository;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {

  @Autowired private TeamRepository teamRepository;

  public TeamDTO createTeam(TeamDTO teamDTO) {
    Team team = new Team(teamDTO.name(), teamDTO.description(), teamDTO.color());
    return convertToDTO(teamRepository.save(team));
  }

  public List<TeamDTO> getAllTeams() {
    return teamRepository.findAll().stream().map(TeamService::convertToDTO).toList();
  }

  public TeamDTO getTeamById(UUID id) {
    return teamRepository
        .findById(id)
        .map(TeamService::convertToDTO)
        .orElseThrow(() -> new TeamNotFoundException(id));
  }

  public TeamDTO updateTeam(UUID id, TeamDTO updatedTeam) {
    return convertToDTO(
        teamRepository
            .findById(id)
            .map(
                team -> {
                  team.setName(updatedTeam.name());
                  team.setDescription(updatedTeam.description());
                  team.setColor(updatedTeam.color());
                  team.setUpdatedAt(Timestamp.from(java.time.Instant.now()));
                  return teamRepository.save(team);
                })
            .orElseThrow(() -> new TeamNotFoundException(id)));
  }

  public void deleteTeam(UUID id) {
    teamRepository.deleteById(id);
  }

  public OrganizationDTO getOrganization(UUID id) {
    return teamRepository
        .findById(id)
        .map(
            team -> {
              if (team.getOrganization() == null) {
                throw new NoOrganizationForTeamException(id);
              } else {
                return OrganizationService.convertToDTO(team.getOrganization());
              }
            })
        .orElseThrow(() -> new TeamNotFoundException(id));
  }

  public static TeamDTO convertToDTO(Team team) {
    return new TeamDTO(
        team.getId(),
        team.getName(),
        team.getDescription(),
        team.getColor(),
        team.getCreatedAt(),
        team.getUpdatedAt());
  }
}
