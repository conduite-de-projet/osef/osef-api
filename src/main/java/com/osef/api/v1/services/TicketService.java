package com.osef.api.v1.services;

import com.osef.api.v1.enums.TicketStatus;
import com.osef.api.v1.exceptions.RepositoryNotFoundException;
import com.osef.api.v1.exceptions.SprintNotFoundException;
import com.osef.api.v1.exceptions.TicketNotFoundException;
import com.osef.api.v1.exceptions.UserNotFoundException;
import com.osef.api.v1.models.Project;
import com.osef.api.v1.models.Repository;
import com.osef.api.v1.models.Sprint;
import com.osef.api.v1.models.Ticket;
import com.osef.api.v1.models.User;
import com.osef.api.v1.models.dto.TicketDTO;
import com.osef.api.v1.models.dto.UserDTO;
import com.osef.api.v1.repositories.ProjectRepository;
import com.osef.api.v1.repositories.RepositoryRepository;
import com.osef.api.v1.repositories.SprintRepository;
import com.osef.api.v1.repositories.TicketRepository;
import com.osef.api.v1.repositories.UserRepository;
import com.osef.api.v1.repositories.specifications.TicketSpecification;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class TicketService {

  @Autowired private TicketRepository ticketRepository;
  @Autowired private ProjectRepository projectRepository;
  @Autowired private UserService userService;
  @Autowired private RepositoryRepository repositoryRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private SprintRepository sprintRepository;

  public TicketDTO createTicket(TicketDTO ticketDTO) {
    Ticket ticket =
        new Ticket(
            ticketDTO.parentTicketId(),
            ticketDTO.type(),
            ticketDTO.status(),
            ticketDTO.title(),
            ticketDTO.description(),
            ticketDTO.priority(),
            ticketDTO.estimatedTime(),
            ticketDTO.timeSpent(),
            ticketDTO.deadline());

    return convertToDTO(ticketRepository.save(ticket));
  }

  public Ticket createTicketRaw(TicketDTO ticketDTO) {
    Ticket ticket =
        new Ticket(
            ticketDTO.parentTicketId(),
            ticketDTO.type(),
            ticketDTO.status(),
            ticketDTO.title(),
            ticketDTO.description(),
            ticketDTO.priority(),
            ticketDTO.estimatedTime(),
            ticketDTO.timeSpent(),
            ticketDTO.deadline());

    return ticketRepository.save(ticket);
  }

  public TicketDTO createTicket(TicketDTO ticketDTO, Project project) {
    Ticket ticket =
        new Ticket(
            ticketDTO.parentTicketId(),
            ticketDTO.type(),
            ticketDTO.status(),
            ticketDTO.title(),
            ticketDTO.description(),
            ticketDTO.priority(),
            ticketDTO.estimatedTime(),
            ticketDTO.timeSpent(),
            ticketDTO.deadline());

    return save(project, ticket);
  }

  public TicketDTO createTicket(
      TicketDTO ticketDTO, Project project, UUID repositoryId, Integer remoteId) {
    // Validate project
    if (project == null) {
      throw new IllegalArgumentException("Project must not be null");
    }

    // Validate repositoryId
    if (repositoryId == null) {
      throw new IllegalArgumentException("Repository ID must not be null");
    }

    Repository repository =
        repositoryRepository
            .findById(repositoryId)
            .orElseThrow(() -> new RepositoryNotFoundException(repositoryId));

    // Retrieve assignee
    User assignee = null;
    if (ticketDTO.assigneeId() != null) {
      assignee =
          userRepository
              .findById(ticketDTO.assigneeId())
              .orElseThrow(() -> new UserNotFoundException(ticketDTO.assigneeId()));
    }

    // Retrieve author
    User author = null;
    if (ticketDTO.authorId() != null) {
      author =
          userRepository
              .findById(ticketDTO.authorId())
              .orElseThrow(() -> new UserNotFoundException(ticketDTO.authorId()));
    }

    // Create ticket
    Ticket ticket =
        new Ticket(
            ticketDTO.parentTicketId(),
            ticketDTO.type(),
            ticketDTO.status(),
            ticketDTO.title(),
            ticketDTO.description(),
            ticketDTO.priority(),
            ticketDTO.estimatedTime(),
            ticketDTO.timeSpent(),
            ticketDTO.deadline());

    // Set additional properties
    ticket.setRemoteId(remoteId);
    ticket.setRepository(repository);
    ticket.setProject(project);
    ticket.setAuthor(author);

    if (assignee != null) {
      ticket.setAssignee(assignee);
    }

    // Save and return
    return save(project, ticket);
  }

  public void deleteTicket(UUID id) {
    ticketRepository.deleteById(id);
  }

  public TicketDTO updateTicket(UUID id, TicketDTO updatedTicket) {
    return convertToDTO(
        ticketRepository
            .findById(id)
            .map(
                ticket -> {
                  TicketStatus originalStatus = ticket.getStatus();
                  if ((originalStatus.equals(TicketStatus.IN_PROGRESS)
                          || originalStatus.equals(TicketStatus.TODO))
                      && updatedTicket.status().equals(TicketStatus.DONE)) {
                    ticket.setDoneAt(Timestamp.from(Instant.now()));
                  }

                  if (originalStatus.equals(TicketStatus.DONE)
                      && !updatedTicket.status().equals(TicketStatus.DONE)) {
                    ticket.setDoneAt(null);
                  }

                  ticket.setParentTicketId(updatedTicket.parentTicketId());
                  ticket.setType(updatedTicket.type());
                  ticket.setStatus(updatedTicket.status());
                  ticket.setTitle(updatedTicket.title());
                  ticket.setDescription(updatedTicket.description());
                  ticket.setPriority(updatedTicket.priority());
                  ticket.setEstimatedTime(updatedTicket.estimatedTime());
                  ticket.setTimeSpent(updatedTicket.timeSpent());
                  ticket.setDeadline(updatedTicket.deadline());

                  if (updatedTicket.sprintId() != null) {
                    Sprint sprint =
                        sprintRepository
                            .findById(updatedTicket.sprintId())
                            .orElseThrow(
                                () -> new SprintNotFoundException(updatedTicket.sprintId()));
                    ticket.setSprint(sprint);
                    sprint.getTickets().add(ticket);

                    sprintRepository.save(sprint);
                  }

                  return ticketRepository.save(ticket);
                })
            .orElseThrow(() -> new TicketNotFoundException(id)));
  }

  public TicketDTO getTicketById(UUID id) {
    return ticketRepository
        .findById(id)
        .map(TicketService::convertToDTO)
        .orElseThrow(() -> new TicketNotFoundException(id));
  }

  public Ticket getTicketByIdRaw(UUID id) {
    return ticketRepository.findById(id).orElseThrow(() -> new TicketNotFoundException(id));
  }

  public List<TicketDTO> getAllTickets() {
    return filterTicket(null, false).stream().map(TicketService::convertToDTO).toList();
  }

  public List<Ticket> getAllRaw(UUID projectId) {
    return filterTicket(null, false);
  }

  public static TicketDTO convertToDTO(Ticket ticket) {
    UUID authorId = (ticket.getAuthor() != null) ? ticket.getAuthor().getId() : null;
    UUID assigneeId = (ticket.getAssignee() != null) ? ticket.getAssignee().getId() : null;
    UUID projectId = (ticket.getProject() != null) ? ticket.getProject().getId() : null;
    UUID sprintId = (ticket.getSprint() != null) ? ticket.getSprint().getId() : null;
    UUID repositoryId = (ticket.getRepository() != null) ? ticket.getRepository().getId() : null;

    return new TicketDTO(
        ticket.getId(),
        ticket.getParentTicketId(),
        authorId,
        assigneeId,
        projectId,
        sprintId,
        repositoryId,
        ticket.getRemoteId(),
        ticket.getCode(),
        ticket.getReference(),
        ticket.getType(),
        ticket.getStatus(),
        ticket.getTitle(),
        ticket.getDescription(),
        ticket.getPriority(),
        ticket.getEstimatedTime(),
        ticket.getTimeSpent(),
        ticket.getDeadline(),
        ticket.getDoneAt(),
        ticket.getCreatedAt(),
        ticket.getUpdatedAt());
  }

  public List<Ticket> filterTicket(TicketStatus status, Boolean recentFirst) {
    Specification<Ticket> spec = Specification.where(null);

    if (status != null) {
      spec = spec.and(TicketSpecification.hasStatus(status.getValue()));
    }

    if (recentFirst != null && recentFirst) {
      spec = spec.and(TicketSpecification.orderByCreatedAtDesc());
    }

    return ticketRepository.findAll(spec);
  }

  public UserDTO getAuthor(UUID id) {
    UUID authorId = getTicketById(id).authorId();
    if (authorId == null) {
      return null;
    }
    return userService.getUserById(authorId);
  }

  public UserDTO getAssignee(UUID id) {
    UUID assigneeId = getTicketById(id).assigneeId();
    if (assigneeId == null) {
      return null;
    }
    return userService.getUserById(assigneeId);
  }

  public static class TicketCodeGenerator {

    public static String generateTicketCode(Project project, Integer reference) {
      String simplifiedName = simplifyName(project.getName());

      if (reference == null) {
        int nextReference = calculateNextReference(project.getTickets().stream().toList());
        return simplifiedName + "-" + nextReference;
      }
      return simplifiedName + "-" + reference;
    }

    public static String simplifyName(String name) {
      name = name.replaceAll("\\s+", "");

      if (name.length() <= 3) {
        return name.toUpperCase();
      }

      String simplifiedName = name.toUpperCase().replaceAll("[AEIOU]", "");
      return simplifiedName.substring(0, Math.min(5, Math.max(3, simplifiedName.length())));
    }

    public static int calculateNextReference(List<Ticket> tickets) {
      return tickets.stream().mapToInt(Ticket::getReference).max().orElse(0) + 1;
    }
  }

  private TicketDTO save(Project project, Ticket ticket) {
    int reference =
        TicketCodeGenerator.calculateNextReference(project.getTickets().stream().toList());
    String code = TicketCodeGenerator.generateTicketCode(project, reference);
    ticket.setCode(code);
    ticket.setReference(reference);

    project.getTickets().add(ticket);
    projectRepository.save(project);

    return convertToDTO(ticket);
  }
}
