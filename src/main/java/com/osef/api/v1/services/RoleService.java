package com.osef.api.v1.services;

import com.osef.api.v1.models.Role;
import com.osef.api.v1.models.dto.RoleDTO;
import com.osef.api.v1.repositories.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

  @Autowired private RoleRepository roleRepository;

  public RoleDTO createRole(RoleDTO roleDTO) {
    Role role = new Role(roleDTO.name(), roleDTO.description(), roleDTO.level());
    roleRepository.save(role);
    return roleDTO;
  }

  public List<RoleDTO> getAllRoles() {
    return roleRepository.findAll().stream().map(RoleService::convertToDTO).toList();
  }

  public static RoleDTO convertToDTO(Role role) {
    return new RoleDTO(role.getId(), role.getName(), role.getDescription(), role.getLevel());
  }
}
