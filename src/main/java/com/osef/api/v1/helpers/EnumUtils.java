package com.osef.api.v1.helpers;

import java.util.concurrent.ThreadLocalRandom;

public class EnumUtils {
  public static <T extends Enum<?>> T getRandomEnumValue(Class<T> enumClass) {
    T[] values = enumClass.getEnumConstants();
    int randomIndex = ThreadLocalRandom.current().nextInt(values.length);
    return values[randomIndex];
  }
}
