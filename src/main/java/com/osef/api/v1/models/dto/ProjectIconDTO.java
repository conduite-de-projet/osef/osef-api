package com.osef.api.v1.models.dto;

public record ProjectIconDTO(String icon, String color) {}
