package com.osef.api.v1.models;

import com.osef.api.v1.enums.TicketPriority;
import com.osef.api.v1.enums.TicketStatus;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Table(name = "tickets")
public class Ticket {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  private UUID parentTicketId;

  @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "ticket")
  private List<TicketLabel> ticketLabels;

  @ManyToOne private User author;

  @ManyToOne private User assignee;

  private String type;
  private TicketStatus status;
  private String title;
  private String description;
  private TicketPriority priority = TicketPriority.VERY_LOW;

  private Integer estimatedTime;
  private Integer timeSpent;
  private Integer reference;
  private Integer remoteId;

  private LocalDate deadline;

  private Timestamp doneAt;
  private Timestamp createdAt;
  private Timestamp updatedAt;
  private String code;

  @ManyToOne private Sprint sprint;

  @ManyToOne private Project project;
  @ManyToOne private Repository repository;

  public Ticket() {}

  public Ticket(
      UUID parentTicketId,
      String type,
      TicketStatus status,
      String title,
      String description,
      TicketPriority priority,
      Integer estimatedTime,
      Integer timeSpent,
      LocalDate deadline,
      User author,
      User assignee) {
    this.parentTicketId = parentTicketId;

    this.type = type;
    this.status = status;
    this.title = title;
    this.description = description;

    this.priority = priority;
    this.estimatedTime = estimatedTime;

    this.timeSpent = timeSpent;
    this.deadline = deadline;

    this.author = author;
    this.assignee = assignee;

    this.createdAt = Timestamp.from(Instant.now());
  }

  public Ticket(
      UUID parentTicketId,
      String type,
      TicketStatus status,
      String title,
      String description,
      TicketPriority priority,
      Integer estimatedTime,
      Integer timeSpent,
      LocalDate deadline) {
    this.parentTicketId = parentTicketId;

    this.type = type;
    this.status = status;
    this.title = title;
    this.description = description;

    this.priority = priority;
    this.estimatedTime = estimatedTime;

    this.timeSpent = timeSpent;
    this.deadline = deadline;

    this.createdAt = Timestamp.from(Instant.now());
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getParentTicketId() {
    return parentTicketId;
  }

  public void setParentTicketId(UUID parentTicketId) {
    this.parentTicketId = parentTicketId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public TicketStatus getStatus() {
    return status;
  }

  public void setStatus(TicketStatus status) {
    this.status = status;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TicketPriority getPriority() {
    return priority;
  }

  public void setPriority(TicketPriority priority) {
    this.priority = priority;
  }

  public Integer getEstimatedTime() {
    return estimatedTime;
  }

  public void setEstimatedTime(Integer estimatedTime) {
    this.estimatedTime = estimatedTime;
  }

  public Integer getTimeSpent() {
    return timeSpent;
  }

  public void setTimeSpent(Integer timeSpent) {
    this.timeSpent = timeSpent;
  }

  public LocalDate getDeadline() {
    return deadline;
  }

  public void setDeadline(LocalDate deadline) {
    this.deadline = deadline;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Sprint getSprint() {
    return sprint;
  }

  public void setSprint(Sprint sprint) {
    this.sprint = sprint;
  }

  public User getAuthor() {
    return author;
  }

  public void setAuthor(User author) {
    this.author = author;
  }

  public User getAssignee() {
    return assignee;
  }

  public void setAssignee(User assignee) {
    this.assignee = assignee;
  }

  public List<TicketLabel> getTicketLabels() {
    return ticketLabels;
  }

  public void setTicketLabels(List<TicketLabel> ticketLabels) {
    this.ticketLabels = ticketLabels;
  }

  @Transient
  public List<Label> getLabels() {
    return ticketLabels.stream().map(TicketLabel::getLabel).collect(Collectors.toList());
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getReference() {
    return reference;
  }

  public void setReference(Integer reference) {
    this.reference = reference;
  }

  public Integer getRemoteId() {
    return remoteId;
  }

  public void setRemoteId(Integer remoteId) {
    this.remoteId = remoteId;
  }

  public Repository getRepository() {
    return repository;
  }

  public void setRepository(Repository repository) {
    this.repository = repository;
  }

  public Timestamp getDoneAt() {
    return doneAt;
  }

  public void setDoneAt(Timestamp doneAt) {
    this.doneAt = doneAt;
  }
}
