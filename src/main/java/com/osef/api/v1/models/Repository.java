package com.osef.api.v1.models;

import com.osef.api.v1.enums.RepositoryType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "repositories")
public class Repository {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Project project;

  private Long remoteId;

  @Enumerated(EnumType.STRING)
  private RepositoryType type;

  private String host;

  private String name;

  private String token;

  private Timestamp tokenExpiringDate;

  private Timestamp lastSynced;

  private Timestamp createdAt;

  private Timestamp updatedAt;

  public Repository() {}

  public Repository(
      Project project,
      Long remoteId,
      RepositoryType type,
      String host,
      String name,
      String token,
      Timestamp tokenExpiringDate,
      Timestamp lastSynced,
      Timestamp createdAt,
      Timestamp updatedAt) {
    this.project = project;
    this.remoteId = remoteId;
    this.type = type;
    this.host = host;
    this.name = name;
    this.token = token;
    this.tokenExpiringDate = tokenExpiringDate;
    this.lastSynced = lastSynced;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Long getRemoteId() {
    return remoteId;
  }

  public void setRemoteId(Long remoteId) {
    this.remoteId = remoteId;
  }

  public RepositoryType getType() {
    return type;
  }

  public void setType(RepositoryType type) {
    this.type = type;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Timestamp getTokenExpiringDate() {
    return tokenExpiringDate;
  }

  public void setTokenExpiringDate(Timestamp tokenExpiringDate) {
    this.tokenExpiringDate = tokenExpiringDate;
  }

  public Timestamp getLastSynced() {
    return lastSynced;
  }

  public void setLastSynced(Timestamp lastSynced) {
    this.lastSynced = lastSynced;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }
}
