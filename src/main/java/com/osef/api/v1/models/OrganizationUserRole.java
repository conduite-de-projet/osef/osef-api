package com.osef.api.v1.models;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;

@Entity
@Table(name = "organization_user_roles")
public class OrganizationUserRole {

  @EmbeddedId private OrganizationUserRoleKey id;

  @ManyToOne
  @MapsId("organizationId")
  @JoinColumn(name = "organization_id")
  private Organization organization;

  @ManyToOne
  @MapsId("userId")
  @JoinColumn(name = "user_id")
  private User user;

  @ManyToOne
  @MapsId("roleId")
  @JoinColumn(name = "role_id")
  private Role role;

  public OrganizationUserRole() {}

  public OrganizationUserRole(OrganizationUserRoleKey id) {
    this.id = id;
  }

  public OrganizationUserRoleKey getId() {
    return id;
  }

  public void setId(OrganizationUserRoleKey id) {
    this.id = id;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }
}
