package com.osef.api.v1.models.dto;

import java.sql.Timestamp;
import java.util.UUID;

public record OrganizationDTO(
    UUID id, String name, String description, Timestamp createdAt, Timestamp updatedAt) {}
