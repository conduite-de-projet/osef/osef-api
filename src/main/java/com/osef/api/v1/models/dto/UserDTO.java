package com.osef.api.v1.models.dto;

import java.sql.Timestamp;
import java.util.UUID;

public record UserDTO(
    UUID id,
    String firstName,
    String lastName,
    String email,
    String passwordHash,
    String profileColor,
    String city,
    String zipCode,
    String address,
    String state,
    String bio,
    String website,
    String status,
    Timestamp createdAt,
    Timestamp updatedAt,
    Timestamp lastLogin) {}
