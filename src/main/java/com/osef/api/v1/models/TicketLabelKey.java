package com.osef.api.v1.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class TicketLabelKey implements Serializable {

  @Column(name = "ticket_id")
  private UUID ticket;

  @Column(name = "label_id")
  private UUID label;

  public TicketLabelKey() {}

  public TicketLabelKey(UUID ticket, UUID label) {
    this.ticket = ticket;
    this.label = label;
  }

  public UUID getTicket() {
    return ticket;
  }

  public void setTicket(UUID ticket) {
    this.ticket = ticket;
  }

  public UUID getLabel() {
    return label;
  }

  public void setLabel(UUID label) {
    this.label = label;
  }

  @Override
  public int hashCode() {
    return Objects.hash(ticket, label);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    TicketLabelKey ticketLabelKey = (TicketLabelKey) obj;
    if (ticketLabelKey == null) {
      return false;
    }

    if (ticket.equals(ticketLabelKey.getTicket()) && label.equals(ticketLabelKey.getLabel())) {
      return true;
    }

    return false;
  }
}
