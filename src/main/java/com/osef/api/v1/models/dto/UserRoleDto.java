package com.osef.api.v1.models.dto;

public record UserRoleDto(UserDTO user, RoleDTO role) {}
