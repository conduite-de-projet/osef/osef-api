package com.osef.api.v1.models.dto;

import java.util.UUID;

public record RoleDTO(UUID id, String name, String description, int level) {}
