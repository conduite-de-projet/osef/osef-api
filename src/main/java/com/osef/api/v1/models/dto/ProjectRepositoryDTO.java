package com.osef.api.v1.models.dto;

import com.osef.api.v1.enums.ProjectStatus;
import com.osef.api.v1.enums.ProjectVisibility;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

public record ProjectRepositoryDTO(
    UUID id,
    String name,
    String description,
    ProjectStatus status,
    Timestamp startDate,
    Timestamp endDate,
    ProjectVisibility visibility,
    Timestamp createdAt,
    Timestamp updatedAt,
    ProjectIconDTO icon,
    Set<RepositoryDTO> repositories) {}
