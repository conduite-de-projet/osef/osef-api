package com.osef.api.v1.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class OrganizationUserRoleKey implements Serializable {

  @Column(name = "organization_id")
  private UUID organizationId;

  @Column(name = "user_id")
  private UUID userId;

  @Column(name = "role_id")
  private UUID roleId;

  public OrganizationUserRoleKey() {}

  public OrganizationUserRoleKey(UUID organizationId, UUID userId, UUID roleId) {
    this.organizationId = organizationId;
    this.userId = userId;
    this.roleId = roleId;
  }

  public UUID getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(UUID organizationId) {
    this.organizationId = organizationId;
  }

  public UUID getUserId() {
    return userId;
  }

  public void setUserId(UUID userId) {
    this.userId = userId;
  }

  public UUID getRoleId() {
    return roleId;
  }

  public void setRoleId(UUID roleId) {
    this.roleId = roleId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(organizationId, userId, roleId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    OrganizationUserRoleKey organizationUserRoleKey = (OrganizationUserRoleKey) obj;
    if (organizationUserRoleKey == null) {
      return false;
    }

    if (organizationId.equals(organizationUserRoleKey.getOrganizationId())
        && userId.equals(organizationUserRoleKey.getUserId())
        && roleId.equals(organizationUserRoleKey.getRoleId())) {
      return true;
    }

    return false;
  }
}
