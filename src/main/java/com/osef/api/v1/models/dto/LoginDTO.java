package com.osef.api.v1.models.dto;

public record LoginDTO(String email, String passwordHash) {}
