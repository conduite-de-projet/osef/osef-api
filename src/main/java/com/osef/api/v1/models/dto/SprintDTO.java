package com.osef.api.v1.models.dto;

import com.osef.api.v1.enums.SprintBlockers;
import com.osef.api.v1.enums.SprintStatus;
import java.sql.Timestamp;
import java.util.UUID;

public record SprintDTO(
    UUID id,
    UUID projectId,
    UUID previousSprintId,
    UUID nextSprintId,
    UUID dependentSprintId,
    Integer reference,
    String name,
    String goals,
    SprintStatus status,
    Timestamp startDate,
    Timestamp actualStartDate,
    Timestamp endDate,
    Timestamp actualEndDate,
    Integer velocity,
    SprintBlockers[] blockers,
    Integer carriedTickets,
    Timestamp createdAt,
    Timestamp updatedAt) {}
