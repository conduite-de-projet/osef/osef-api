package com.osef.api.v1.models.dto;

import java.sql.Timestamp;
import java.util.UUID;

public record TeamDTO(
    UUID id,
    String name,
    String description,
    String color,
    Timestamp createdAt,
    Timestamp updatedAt) {}
