package com.osef.api.v1.models;

import com.osef.api.v1.enums.ProjectStatus;
import com.osef.api.v1.enums.ProjectVisibility;
import com.osef.api.v1.models.dto.ProjectIconDTO;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "projects")
public class Project {
  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  private Organization organization;

  private String name;
  private String description;

  @Enumerated(EnumType.STRING)
  private ProjectStatus status;

  private Timestamp startDate;
  private Timestamp endDate;

  @Enumerated(EnumType.STRING)
  private ProjectVisibility visibility;

  private String iconName;
  private String iconColor;

  private Timestamp createdAt;
  private Timestamp updatedAt;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "project")
  @OrderBy("reference DESC")
  private Set<Sprint> sprints = new HashSet<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "project")
  @OrderBy("reference ASC")
  private Set<Ticket> tickets = new HashSet<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "project")
  private Set<Repository> repositories = new HashSet<>();

  private String icon;

  public Project() {}

  public Project(
      String name,
      ProjectStatus status,
      Timestamp startDate,
      ProjectVisibility visibility,
      Timestamp createdAt,
      Timestamp updatedAt,
      String description,
      Timestamp endDate,
      Set<Sprint> sprints,
      Set<Ticket> tickets,
      Set<Repository> repositories,
      ProjectIconDTO icon) {

    this.organization = null;
    this.name = name;
    this.description = description;
    this.status = status;
    this.startDate = startDate;
    this.endDate = endDate;
    this.visibility = visibility;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.sprints = sprints;
    this.tickets = tickets;
    this.repositories = repositories;
    this.setIcon(icon);
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ProjectStatus getStatus() {
    return status;
  }

  public void setStatus(ProjectStatus status) {
    this.status = status;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  public ProjectVisibility getVisibility() {
    return visibility;
  }

  public void setVisibility(ProjectVisibility visibility) {
    this.visibility = visibility;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Set<Sprint> getSprints() {
    return sprints;
  }

  public void setSprints(Set<Sprint> sprints) {
    this.sprints = sprints;
  }

  public Set<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(Set<Ticket> tickets) {
    this.tickets = tickets;
  }

  public Set<Repository> getRepositories() {
    return repositories;
  }

  public void setRepositories(Set<Repository> repositories) {
    this.repositories = repositories;
  }

  public ProjectIconDTO getIcon() {
    return new ProjectIconDTO(iconName, iconColor);
  }

  public void setIcon(ProjectIconDTO icon) {
    this.iconName = icon.icon();
    this.iconColor = icon.color();
  }
}
