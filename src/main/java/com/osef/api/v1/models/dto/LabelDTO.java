package com.osef.api.v1.models.dto;

import java.util.UUID;

public record LabelDTO(UUID id, String name, String color) {}
