package com.osef.api.v1.models;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class TeamUserKey implements Serializable {

  @Column(name = "team_id")
  private UUID teamId;

  @Column(name = "user_id")
  private UUID userId;

  public TeamUserKey() {}

  public TeamUserKey(UUID teamId, UUID userId) {
    this.teamId = teamId;
    this.userId = userId;
  }

  public UUID getTeamId() {
    return teamId;
  }

  public UUID getUserId() {
    return userId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(teamId, userId);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    TeamUserKey teamUserKey = (TeamUserKey) obj;
    if (teamUserKey == null) {
      return false;
    }

    if (teamId.equals(teamUserKey.getTeamId()) && userId.equals(teamUserKey.getUserId())) {
      return true;
    }

    return false;
  }
}
