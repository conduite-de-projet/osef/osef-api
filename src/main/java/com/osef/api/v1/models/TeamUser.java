package com.osef.api.v1.models;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
public class TeamUser {
  @EmbeddedId private TeamUserKey id;

  @ManyToOne
  @MapsId("teamId")
  @JoinColumn(name = "team_id")
  private Team team;

  @ManyToOne
  @MapsId("userId")
  @JoinColumn(name = "user_id")
  private User user;

  private Timestamp joinedAt;

  public TeamUser() {}

  public TeamUser(TeamUserKey id) {
    this.id = id;
    this.joinedAt = Timestamp.from(Instant.now());
  }

  public TeamUserKey getId() {
    return id;
  }

  public void setId(TeamUserKey id) {
    this.id = id;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Timestamp getJoinedAt() {
    return joinedAt;
  }

  public void setJoinedAt(Timestamp joinedAt) {
    this.joinedAt = joinedAt;
  }
}
