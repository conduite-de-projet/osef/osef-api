package com.osef.api.v1.models;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.Table;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Table(name = "ticket_labels")
public class TicketLabel {
  @EmbeddedId private TicketLabelKey id;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("ticketId")
  @JoinColumn(name = "ticket_id")
  private Ticket ticket;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId("labelId")
  @JoinColumn(name = "label_id")
  private Label label;

  private Timestamp createdAt;

  public TicketLabel() {}

  public TicketLabel(TicketLabelKey id) {
    this.id = id;
    createdAt = Timestamp.from(Instant.now());
  }

  public TicketLabelKey getId() {
    return id;
  }

  public void setId(TicketLabelKey id) {
    this.id = id;
  }

  public Ticket getTicket() {
    return ticket;
  }

  public void setTicket(Ticket ticket) {
    this.ticket = ticket;
  }

  public Label getLabel() {
    return label;
  }

  public void setLabel(Label label) {
    this.label = label;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }
}
