package com.osef.api.v1.models.dto;

import com.osef.api.v1.enums.RepositoryType;
import java.sql.Timestamp;
import java.util.UUID;

public record RepositoryDTO(
    UUID id,
    UUID projectId,
    Long remoteId,
    RepositoryType type,
    String host,
    String name,
    String token,
    Timestamp tokenExpiringDate,
    Timestamp lastSynced,
    Timestamp createdAt,
    Timestamp updatedAt) {}
