package com.osef.api.v1.models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @OneToMany(
      orphanRemoval = true,
      cascade = {CascadeType.REMOVE},
      mappedBy = "author",
      fetch = FetchType.LAZY)
  private Set<Ticket> tickets;

  @OneToMany(
      orphanRemoval = true,
      cascade = {CascadeType.REMOVE},
      mappedBy = "assignee",
      fetch = FetchType.LAZY)
  private Set<Ticket> assignedTickets;

  @OneToMany(
      orphanRemoval = true,
      cascade = {CascadeType.REMOVE},
      mappedBy = "user",
      fetch = FetchType.LAZY)
  private Set<TeamUser> teams;

  @OneToMany(
      orphanRemoval = true,
      cascade = {CascadeType.REMOVE},
      mappedBy = "user",
      fetch = FetchType.LAZY)
  private Set<OrganizationUserRole> roles;

  private String firstName;
  private String lastName;

  @Column(name = "email", nullable = false, unique = true)
  private String email;

  private String profileColor;
  private String city;
  private String zipCode;
  private String address;
  private String state;
  private String bio;
  private String website;

  private String status;

  private byte[] passwordHash;
  private byte[] salt;

  private Timestamp createdAt;
  private Timestamp updatedAt;
  private Timestamp lastLogin;

  public User() {}

  public User(
      String firstName,
      String lastName,
      String email,
      byte[] passwordHash,
      byte[] salt,
      String profileColor,
      String city,
      String zipCode,
      String address,
      String state,
      String bio,
      String website,
      String status) {

    this.passwordHash = passwordHash;
    this.salt = salt;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.profileColor = profileColor;
    this.city = city;
    this.zipCode = zipCode;
    this.address = address;
    this.state = state;
    this.bio = bio;
    this.website = website;
    this.status = status;

    this.teams = new HashSet<>();
    this.roles = new HashSet<>();
    this.tickets = new HashSet<>();
    this.assignedTickets = new HashSet<>();

    this.createdAt = Timestamp.from(Instant.now());
    this.lastLogin = Timestamp.from(Instant.now());
  }

  public UUID getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public byte[] getPasswordHash() {
    return passwordHash;
  }

  public void setPasswordHash(byte[] passwordHash) {
    this.passwordHash = passwordHash;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Timestamp getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(Timestamp lastLogin) {
    this.lastLogin = lastLogin;
  }

  public void setFirstName(String name) {
    this.firstName = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Set<TeamUser> getTeams() {
    return teams;
  }

  public void setTeams(Set<TeamUser> teams) {
    this.teams = teams;
  }

  public Set<OrganizationUserRole> getRoles() {
    return roles;
  }

  public void setRoles(Set<OrganizationUserRole> roles) {
    this.roles = roles;
  }

  public Set<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(Set<Ticket> tickets) {
    this.tickets = tickets;
  }

  public Set<Ticket> getAssignedTickets() {
    return assignedTickets;
  }

  public void setAssignedTickets(Set<Ticket> assignedTickets) {
    this.assignedTickets = assignedTickets;
  }

  public byte[] getSalt() {
    return salt;
  }

  public void setSalt(byte[] salt) {
    this.salt = salt;
  }

  public String getProfileColor() {
    return this.profileColor;
  }

  public void setProfileColor(String profileColor) {
    this.profileColor = profileColor;
  }

  public String getCity() {
    return this.city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZipCode() {
    return this.zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getState() {
    return this.state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getBio() {
    return this.bio;
  }

  public void setBio(String bio) {
    this.bio = bio;
  }

  public String getWebsite() {
    return this.website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }
}
