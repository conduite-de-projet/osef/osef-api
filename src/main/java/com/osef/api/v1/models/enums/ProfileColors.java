package com.osef.api.v1.models.enums;

public enum ProfileColors {
  ORANGE,
  RED,
  BLUE,
  YELLOW,
  PURPLE,
}
