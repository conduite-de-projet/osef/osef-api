package com.osef.api.v1.models;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@Entity
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  private String name;
  private String description;

  private int level;

  @OneToMany(
      orphanRemoval = true,
      cascade = {CascadeType.REMOVE},
      mappedBy = "role",
      fetch = FetchType.LAZY)
  private Set<OrganizationUserRole> roles;

  private Timestamp createdAt;
  private Timestamp updatedAt;

  public Role() {}

  public Role(String name, String description, int level) {
    this.name = name;
    this.description = description;
    this.level = level;
    this.createdAt = Timestamp.from(java.time.Instant.now());
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Set<OrganizationUserRole> getRoles() {
    return roles;
  }

  public void setRoles(Set<OrganizationUserRole> roles) {
    this.roles = roles;
  }
}
