package com.osef.api.v1.models.dto;

import com.osef.api.v1.enums.TicketPriority;
import com.osef.api.v1.enums.TicketStatus;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

public record TicketDTO(
    UUID id,
    UUID parentTicketId,
    UUID authorId,
    UUID assigneeId,
    UUID projectId,
    UUID sprintId,
    UUID repositoryId,
    Integer remoteId,
    String code,
    Integer reference,
    String type,
    TicketStatus status,
    String title,
    String description,
    TicketPriority priority,
    Integer estimatedTime,
    Integer timeSpent,
    LocalDate deadline,
    Timestamp doneAt,
    Timestamp createdAt,
    Timestamp updatedAt) {}
