package com.osef.api.v1.models;

import com.osef.api.v1.enums.SprintBlockers;
import com.osef.api.v1.enums.SprintStatus;
import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(
    name = "sprints",
    indexes = {@Index(name = "idx_sprint_reference", columnList = "reference")})
public class Sprint {

  @Id
  @GeneratedValue(strategy = GenerationType.UUID)
  private UUID id;

  @Column(nullable = false)
  private Integer reference;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Project project;

  private String name;
  private String goals;

  @Enumerated(EnumType.STRING)
  private SprintStatus status;

  private Timestamp startDate;
  private Timestamp actualStartDate;
  private Timestamp endDate;
  private Timestamp actualEndDate;

  private Integer velocity;
  private Integer carriedTickets;

  private Timestamp createdAt;
  private Timestamp updatedAt;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "sprint")
  @OrderBy("reference ASC")
  private Set<Ticket> tickets = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY)
  private Sprint previousSprint;

  @ManyToOne(fetch = FetchType.LAZY)
  private Sprint nextSprint;

  @ManyToOne(fetch = FetchType.LAZY)
  private Sprint dependentSprint;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "sprint_blockers", joinColumns = @JoinColumn(name = "sprint_id"))
  private Set<SprintBlockers> blockers = new HashSet<>();

  @PrePersist
  private void setReference() {
    if (project != null) {
      int currentMaxReference =
          project.getSprints().stream()
              .filter(sprint -> sprint.getReference() != null)
              .mapToInt(Sprint::getReference)
              .max()
              .orElse(0);
      this.reference = currentMaxReference + 1;
    } else {
      throw new IllegalStateException("Sprint must be linked to a project");
    }
  }

  public Sprint() {}

  public Sprint(
      Project project,
      String name,
      String goals,
      SprintStatus status,
      Timestamp startDate,
      Timestamp endDate,
      Integer velocity,
      Sprint previousSprint,
      Sprint nextSprint,
      Sprint dependentSprint) {
    this.project = project;
    this.name = name;
    this.goals = goals;
    this.status = status;
    this.startDate = startDate;
    this.endDate = endDate;
    this.actualStartDate = null;
    this.actualEndDate = null;
    this.velocity = velocity;
    this.previousSprint = previousSprint;
    this.nextSprint = nextSprint;
    this.dependentSprint = dependentSprint;
    this.createdAt = Timestamp.from(Instant.now());
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGoals() {
    return goals;
  }

  public void setGoals(String goals) {
    this.goals = goals;
  }

  public SprintStatus getStatus() {
    return status;
  }

  public void setStatus(SprintStatus status) {
    this.status = status;
  }

  public Timestamp getStartDate() {
    return startDate;
  }

  public void setStartDate(Timestamp startDate) {
    this.startDate = startDate;
  }

  public Timestamp getActualStartDate() {
    return actualStartDate;
  }

  public void setActualStartDate(Timestamp actualStartDate) {
    this.actualStartDate = actualStartDate;
  }

  public Timestamp getEndDate() {
    return endDate;
  }

  public void setEndDate(Timestamp endDate) {
    this.endDate = endDate;
  }

  public Timestamp getActualEndDate() {
    return actualEndDate;
  }

  public void setActualEndDate(Timestamp actualEndDate) {
    this.actualEndDate = actualEndDate;
  }

  public Integer getVelocity() {
    return velocity;
  }

  public void setVelocity(Integer velocity) {
    this.velocity = velocity;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Set<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(Set<Ticket> tickets) {
    this.tickets = tickets;
  }

  public Sprint getPreviousSprint() {
    return previousSprint;
  }

  public void setPreviousSprint(Sprint previousSprint) {
    this.previousSprint = previousSprint;
  }

  public Sprint getNextSprint() {
    return nextSprint;
  }

  public void setNextSprint(Sprint nextSprint) {
    this.nextSprint = nextSprint;
  }

  public Sprint getDependentSprint() {
    return dependentSprint;
  }

  public void setDependentSprint(Sprint dependentSprint) {
    this.dependentSprint = dependentSprint;
  }

  public Set<SprintBlockers> getBlockers() {
    return blockers;
  }

  public void setBlockers(Set<SprintBlockers> blockers) {
    this.blockers = blockers;
  }

  public Integer getReference() {
    return reference;
  }

  public Integer getCarriedTickets() {
    return carriedTickets;
  }

  public void setCarriedTickets(Integer carriedTickets) {
    this.carriedTickets = carriedTickets;
  }
}
