package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TicketStatus {
  TODO("TODO"),
  IN_PROGRESS("IN_PROGRESS"),
  DONE("DONE");

  private final String value;

  TicketStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value;
  }

  @JsonCreator
  public static TicketStatus fromValue(String value) {
    for (TicketStatus status : TicketStatus.values()) {
      if (status.value.equalsIgnoreCase(value)) {
        return status;
      }
    }
    throw new IllegalArgumentException("Unknown TicketPriority: " + value);
  }
}
