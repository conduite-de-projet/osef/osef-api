package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum SprintStatus {
  PLANNED("planned"),
  IN_PROGRESS("in_progress"),
  COMPLETED("completed");

  private final String value;

  SprintStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value;
  }

  @JsonCreator
  public static SprintStatus fromValue(String value) {
    for (SprintStatus status : SprintStatus.values()) {
      if (status.value.equalsIgnoreCase(value)) {
        return status;
      }
    }
    throw new IllegalArgumentException("Unknown SprintStatus: " + value);
  }
}
