package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RepositoryType {
  GITLAB("gitlab"),
  GITHUB("github"),
  GITLAB_HOSTED("gitlab-hosted");

  private final String value;

  RepositoryType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value;
  }

  @JsonCreator
  public static RepositoryType fromValue(String value) {
    for (RepositoryType type : RepositoryType.values()) {
      if (type.value.equalsIgnoreCase(value)) {
        return type;
      }
    }
    throw new IllegalArgumentException("Unknown RepositoryType: " + value);
  }
}
