package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProjectVisibility {
  PRIVATE("private"),
  PUBLIC("public"),
  INTERNAL("internal");

  private final String value;

  ProjectVisibility(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value;
  }

  @JsonCreator
  public static ProjectVisibility fromValue(String value) {
    for (ProjectVisibility visibility : ProjectVisibility.values()) {
      if (visibility.value.equalsIgnoreCase(value)) {
        return visibility;
      }
    }
    throw new IllegalArgumentException("Unknown ProjectVisibility: " + value);
  }
}
