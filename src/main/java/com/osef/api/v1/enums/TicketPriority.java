package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TicketPriority {
  VERY_LOW("vl"),
  LOW("l"),
  MEDIUM("m"),
  HIGH("h"),
  VERY_HIGH("vh"),
  HIGHEST("hh");

  private final String value;

  TicketPriority(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value;
  }

  @JsonCreator
  public static TicketPriority fromValue(String value) {
    for (TicketPriority priority : TicketPriority.values()) {
      if (priority.value.equalsIgnoreCase(value)) {
        return priority;
      }
    }
    throw new IllegalArgumentException("Unknown TicketPriority: " + value);
  }
}
