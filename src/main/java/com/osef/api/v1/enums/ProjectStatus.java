package com.osef.api.v1.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ProjectStatus {
  ACTIVE("active"),
  INACTIVE("inactive"),
  COMPLETED("completed");

  private final String value;

  ProjectStatus(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  @JsonValue
  public String toString() {
    return value; // Utilisé lors de la sérialisation
  }

  @JsonCreator
  public static ProjectStatus fromValue(String value) {
    for (ProjectStatus status : ProjectStatus.values()) {
      if (status.value.equalsIgnoreCase(value)) {
        return status;
      }
    }
    throw new IllegalArgumentException("Unknown ProjectStatus: " + value);
  }
}
