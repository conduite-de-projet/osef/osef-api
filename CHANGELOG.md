# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2024-12-15

### Added

- Created data model for tickets and labels
- Created repositories for tickets and labels
- Created routes model for tickets and labels
- Created seeders model for tickets and labels
- Relation between label and ticket
- User authentication
- Basic integration tests
- Sprint statistics

### Changed

- Data model for users
- Data model for sprints
- Data model for tickets
- Ticket Service
- Sprint Service
- Projects Controller
- Ticket Repository
- Project Repository
- Sprint Repository

### Fixed
- Missing User email unique constraint
- Project icon update

## [0.2.0] - 2024-12-02

### Added

- Initialized Spotless, Checkstyle
- Created data model for teams, organizations, roles and pivot models
- Created repositories for teams, organizations, roles and pivot models
- Created routes for teams, organizations and roles
- Created seeders for teams, organizations and roles
- Relation between user and team
- Relation between team and organizations
- Relation between user, organization and role
 
### Changed

- Users data model
- Users routes

## [0.1.0] - 2024-11-18

### Added

- Project setup (java-spring, hibernate and flyweight)
- Initialized Lefthook
- Gitlab CI
- Created data model for users
- Created routes for users

[unreleased]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-api/
[0.1.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-api/-/tags/0.1.0
[0.2.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-api/-/tags/0.2.0
[0.3.0]: https://gitlab.emi.u-bordeaux.fr/conduite-de-projet/osef/osef-api/-/tags/0.3.0
